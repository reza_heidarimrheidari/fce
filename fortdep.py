#!/usr/bin/python

'''
@author: Mohammad Reza Heidari
'''

from staticprogramanalysis import StaticProgramAnalysis

def fortModuleProperties():
    staticProgramAnalysis = StaticProgramAnalysis()
    staticProgramAnalysis.printModuleProperties()

if __name__ == "__main__":
    fortModuleProperties() 
