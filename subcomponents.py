# coding=utf8
'''                                                                                                                                                                                                                                           @author: Mohammad Reza Heidari                                                                                                                                                                                                                
'''

from superset         import SupersetSubcomponentsExtraction
from minimal          import MinimalSubcomponentsExtraction
from subcomponentsloc import SubcomponentsLOCExtraction

class SubcomponentsExtraction():
      def __init__(self, sourceFiles, ndg, analysisConfig, graphBuilder, program):
          self.sourceFiles    = sourceFiles
          self.ndg            = ndg 
          self.analysisConfig = analysisConfig
          self.graphBuilder   = graphBuilder
          self.program        = program 

      def getResults(self):
          supersetSubcomponentsExtraction = SupersetSubcomponentsExtraction (self.sourceFiles, self.ndg, self.analysisConfig, self.graphBuilder, self.program)
          minimalSubcomponentsExtraction  = MinimalSubcomponentsExtraction  (self.sourceFiles, self.ndg, self.analysisConfig, self.graphBuilder, self.program)
          subcomponentsLOCExtraction      = SubcomponentsLOCExtraction      (self.sourceFiles,           self.analysisConfig,                    self.program)

          supersetSubcomponentsExtraction.getResults()
          minimalSubcomponentsExtraction.getResults ()
          subcomponentsLOCExtraction.getResults     ()

      def loadResults(self):
          minimalSubcomponentsExtraction  = MinimalSubcomponentsExtraction (self.sourceFiles, self.ndg, self.analysisConfig, self.graphBuilder, self.program)
          minimalSubcomponentsExtraction.loadResults()


      def printResultsOverview(self):
          supersetSubcomponentsExtraction = SupersetSubcomponentsExtraction (self.sourceFiles, self.ndg, self.analysisConfig, self.graphBuilder, self.program)
          supersetSubcomponentsExtraction.printResultsOverview()

      def printSupersetSubcomponents(self):
          supersetSubcomponentsExtraction = SupersetSubcomponentsExtraction (self.sourceFiles, self.ndg, self.analysisConfig, self.graphBuilder, self.program)
          supersetSubcomponentsExtraction.printResults()

          subcomponentsLOCExtraction = SubcomponentsLOCExtraction(self.sourceFiles, self.analysisConfig, self.program)
          subcomponentsLOCExtraction.printSupersetLOC()

      def printMinimalSubcomponents(self):
          minimalSubcomponentsExtraction = MinimalSubcomponentsExtraction (self.sourceFiles, self.ndg, self.analysisConfig, self.graphBuilder, self.program)
          minimalSubcomponentsExtraction.printResults()

          subcomponentsLOCExtraction = SubcomponentsLOCExtraction(self.sourceFiles, self.analysisConfig, self.program)
          subcomponentsLOCExtraction.printRequiredLOC()
