#!/usr/bin/python
'''
@author: Mohammad Reza Heidari
'''

from staticprogramanalysis import StaticProgramAnalysis

def fortanalysis():
    staticProgramAnalysis = StaticProgramAnalysis()
    staticProgramAnalysis.getSubcomponents()

    staticProgramAnalysis.printResults_SubcomponentsOverview()

def fortModuleProperties():
    staticProgramAnalysis = StaticProgramAnalysis()
    staticProgramAnalysis.printModuleProperties()

if __name__ == "__main__":
    fortanalysis()
