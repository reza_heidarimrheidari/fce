# coding=utf8

import re
from collections import defaultdict

class Node:
    def __init__(self):
        self.visited=False
        self.peers  =[]
        return

class DAG:
    def __init__(self):
        self.node=defaultdict(Node)
        
    def addEdge(self,namespace1,namespace2):
                          
        if (self.node[namespace1].peers.count(namespace2)<1):
            self.node[namespace1].peers.append(namespace2)
            
        keys = [key for key in self.node]
        if (keys.count(namespace2)<1):
            self.node[namespace2]=Node()

class NDG(DAG):
    def __init__(self):
        self.node=defaultdict(Node)
        self.visited_topnamespaces=[]
        self.stack=[]
        
    def addEdge(self,namespace1,namespace2):
         
        if namespace1.lower() == namespace2.lower():
            keys = [key for key in self.node]
            if (keys.count(namespace1)<1):
                self.node[namespace1]=Node()
            return  
            
        if (self.node[namespace1].peers.count(namespace2)<1):
            self.node[namespace1].peers.append(namespace2)
            
        keys = [key for key in self.node]
        if (keys.count(namespace2)<1):
            self.node[namespace2]=Node()

    def placement(self,namespace):
        if (self.node[namespace].visited):
            return

        for peernamespace in self.node[namespace].peers:
            self.placement(peernamespace)
                           
        self.stack.insert(0,namespace)
        self.node[namespace].visited=True
        return
    
    def topologocalsort(self,startnamespace):
        self.stack=[]
        for namespace in self.node:
            self.node[namespace].visited=False
        
        keys = [key for key in self.node]
        if (keys.count(startnamespace)>0):
            self.placement(startnamespace)
        return(self.stack)
    
    def printTopologocallySortedNamespaces(self,startnamespace):
        self.topologocalsort(startnamespace)
        for namespace in self.stack:
            print("* ",namespace)

    def pick_topnamespace(self):
        flag=False
        topnamespace=''
        for namespace in self.stack:
            if self.visited_topnamespaces.count(namespace)<=0:
                flag=True
                topnamespace=namespace
                self.visited_topnamespaces.append(namespace)
                break                    
        return flag,topnamespace

    def printNamespaces(self):
        for nameSpace in self.node.keys():
            print("*** ",nameSpace)
        return(self.node.keys())
