# coding=utf8
'''
@author: Mohammad Reza Heidari
'''

from printout                  import printWarning
from modulepropertiesextractor import ModulesPropertiesExtractor, REQUIREDLOC_WRT_CARVEDOUTPROGRAM, REQUIREDLOC_WRT_COMPONENT, SHAREDLOC, ORIGINALLOC
from source                    import ORIGINALCODESNAME, COMPONENTCODESNAME, CARVEDOUTPROGRAMCODESNAME, SHAREDCODESNAME, ComponentImaginaryMasterEntrypointNameSpace

class SubcomponentsLOCExtraction():
      def __init__(self, sourceFiles, analysisConfig, program):
          self.__sourceFiles    = sourceFiles
          self.__analysisConfig = analysisConfig
          self.__program        = program

          self.__analysisConfig   = analysisConfig
          self.__component        = self.__program.component 
          self.__carvedoutProgram = self.__program.carvedoutProgram
          self.__moduleName       = None
          self.__module           = None

          self.dataFilePath             = self.__analysisConfig.getDataPath()                                                                  
          self.subcomponentsLOCFileName = 'subcomponents'
          self.locFileExtension         = '.loc'

      def getResults(self):
          self.modulesPropertiesExtractor = ModulesPropertiesExtractor(self.__sourceFiles, self.__analysisConfig, self.__program)
          self.modulesPropertiesExtractor.getResults()
          
          self.getSubcomponentsSuperSetLOC()
          self.getSubcomponentsRequiredLOC()
          self.storeResults()

      def loadResults(self):
          self.deserialize()

      def storeResults(self):
          self.serialize()

      def serialize(self):
          import pickle
          loc    = [0,0,0,0,0,0]
          loc[0] = self.__program.carvedoutProgram.LOC.superSet
          loc[1] = self.__program.component.LOC.superSet      
          loc[2] = self.__program.sharedCodes.LOC.superSet 
          
          loc[3] = self.__program.carvedoutProgram.LOC.required
          loc[4] = self.__program.component.LOC.required
          loc[5] = self.__program.sharedCodes.LOC.required

          subcomponentsLOCFullFilename = self.dataFilePath + self.subcomponentsLOCFileName + self.locFileExtension
            
          with open(subcomponentsLOCFullFilename, 'wb') as fp:
               pickle.dump(loc, fp)

      def deserialize(self):
          import pickle

          subcomponentsLOCFullFilename = self.dataFilePath + self.subcomponentsLOCFileName + self.locFileExtension
          with open(subcomponentsLOCFullFilename, 'rb') as fp:
               loc = pickle.load(fp)

          self.__program.carvedoutProgram.LOC.superSet = loc[0]
          self.__program.component.LOC.superSet        = loc[1]
          self.__program.sharedCodes.LOC.superSet      = loc[2]
                                                               
          self.__program.carvedoutProgram.LOC.required = loc[3]
          self.__program.component.LOC.required        = loc[4]
          self.__program.sharedCodes.LOC.required      = loc[5]

      def printSupersetLOC(self):
          self.loadResults()

          print("\n\n" )
          print("*****  CarvedoutProgram Superset LOC: ", int(self.__program.carvedoutProgram.LOC.superSet))
          print("*****  Component        Superset LOC: ", int(self.__program.component.LOC.superSet       ))
          print("*****  Shared           SuperSet LOC: ", int(self.__program.sharedCodes.LOC.superSet     ))

      def printRequiredLOC(self):
          self.loadResults()

          print("\n\n" )
          print("***** CarvedoutProgram Required LOC: ", int(self.__program.carvedoutProgram.LOC.required)) 
          print("***** Component        Required LOC: ", int(self.__program.component.LOC.required       )) 
          print("***** Shared           Required LOC: ", int(self.__program.sharedCodes.LOC.required     )) 

      def getSubcomponentsSuperSetLOC(self):
          self.getSubcomponentSuperSetLOC(self.__program.component       )
          self.getSubcomponentSuperSetLOC(self.__program.carvedoutProgram)
          self.getSubcomponentSuperSetLOC(self.__program.sharedCodes     )
     
      def getSubcomponentsRequiredLOC(self):
          self.getSubcomponentRequiredLOC(self.__program.component,      )
          self.getSubcomponentRequiredLOC(self.__program.carvedoutProgram)
          self.getSubcomponentRequiredLOC(self.__program.sharedCodes     )

      def getSubcomponentSuperSetLOC(self, subComponent):
          subComponent.LOC.superSet = 0
          for namespaceName in set(subComponent.namespaces.superSet):
              subComponent.LOC.superSet = subComponent.LOC.superSet + self.getNamespaceSuperSetLOC(namespaceName)
          return(subComponent.LOC.superSet)

      def getSubcomponentRequiredLOC(self, subComponent):
          subComponent.LOC.required = 0
          for namespaceName in set(subComponent.namespaces.required):
              subComponent.LOC.required = subComponent.LOC.required + self.getNamespacesRequiredLOC(namespaceName, subComponent.name)

          return(subComponent.LOC.required)

      def getNamespaceSuperSetLOC(self, namespaceName):
          if namespaceName == ComponentImaginaryMasterEntrypointNameSpace:
              return(0)

          namespace = self.getSuperSetNamespace(namespaceName)
          if namespace is None:
              return(0)
          else:
              return(self.__program.namespaces[namespaceName].LOC[ORIGINALLOC])

      def getNamespacesRequiredLOC(self, namespaceName, subComponentType):
          namespaceName = self.__sourceFiles.getNamespaceName_from_nameSpacePrintingName(namespaceName)
          if namespaceName is None:
              return(0)
          else:  
              if subComponentType == COMPONENTCODESNAME:
                  return(self.__program.namespaces[namespaceName].LOC[REQUIREDLOC_WRT_COMPONENT])

              elif subComponentType == CARVEDOUTPROGRAMCODESNAME:
                  return(self.__program.namespaces[namespaceName].LOC[REQUIREDLOC_WRT_CARVEDOUTPROGRAM])

              elif subComponentType == SHAREDCODESNAME: 
                  return(self.__program.namespaces[namespaceName].LOC[SHAREDLOC])

              else:  
                  printWarning('Namespace not recognisable : ' + str(namespaceName), 'subcomponentsloc.py')
                  return(0)

      def getSuperSetNamespace(self, namespaceName):
          return(namespaceName) 
