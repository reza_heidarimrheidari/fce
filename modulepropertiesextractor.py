# coding=utf8
'''
@author: Mohammad Reza Heidari
'''

import re
from assertions        import assertType, assertTypeAll
from typefinder        import TypeFinder
from printout          import printWarning
from source            import FortranProgramMainUnit, FortranProgramGlobalNameSpace
from wrongdependencies import ModuleWrongDependencyDetector

ORIGINALLOC                       = 0
REQUIREDLOC_WRT_CARVEDOUTPROGRAM  = 1 
REQUIREDLOC_WRT_COMPONENT         = 2
DEDICATEDLOC_WRT_CARVEDOUTPROGRAM = 3 
DEDICATEDLOC_WRT_COMPONENT        = 4
SHAREDLOC                         = 5
DEADLOC                           = 6

class ModulesPropertiesExtractor():
    def __init__(self, sourceFiles, analysisConfig, program):
        self.__sourceFiles                                      = sourceFiles
        self.__analysisConfig                                   = analysisConfig
        self.__program                                          = program        

        self.__component                                        = self.__program.component 
        self.__carvedoutProgram                                 = self.__program.carvedoutProgram

        self.__moduleSharedSubroutines                          = set()
        self.__moduleSharedGlobalVariables                      = set() 
        self.__moduleSharedTypes                                = set()
        self.__moduleSharedParameters                           = set()

        self.__moduleSubroutinesDedicatedToCarvedoutProgram     = set()
        self.__moduleGlobalVariablesDedicatedToCarvedoutProgram = set()
        self.__moduleTypesDedicatedToCarvedoutProgram           = set()
        self.__moduleParametersDedicatedToCarvedoutProgram      = set()

        self.__moduleSubroutinesDedicatedToComponent            = set()
        self.__moduleGlobalVariablesDedicatedToComponent        = set()
        self.__moduleTypesDedicatedToComponent                  = set()
        self.__moduleParametersDedicatedToComponent             = set()

        self.__moduleDeadSubroutines                            = set()
        self.__moduleDeadGlobalVariables                        = set()
        self.__moduleDeadTypes                                  = set()
        self.__moduleDeadParameters                             = set()

        self.__moduleDedicatedLOC_to_carvedoutProgram           = 0
        self.__moduleDedicatedLOC_to_component                  = 0 
        self.__moduleSharedLOC                                  = 0  
        self.__moduleDeadLOC                                    = 0
        self.__moduleOriginalLOC                                = 0

        self.dataFilePath                                       = self.__analysisConfig.getDataPath()
                                                                
        self.subroutinesExtension                               = '_subs'
        self.variablesExtension                                 = '_vars'
        self.parametersExtension                                = '_pars'
        self.typesExtension                                     = '_types'
        self.interfacesExtension                                = '_interfaces'
                                                                
        self.dedicatedCodesToCarvedoutProgramFileExtension      = '.cp'
        self.dedicatedCodesToComponentFileExtension             = '.com'
        self.sharedCodesFileExtension                           = '.shd'
        self.deadCodesFileExtension                             = '.ded'
        self.locFileExtension                                   = '.loc'

    def getResults(self):
        supersetNamespaces = set()
        supersetNamespaces.update(self.__component.namespaces.superSet       )
        supersetNamespaces.update(self.__carvedoutProgram.namespaces.superSet)

        for supersetNamespace in supersetNamespaces:
            self.getSupersetNamespaceProperties(supersetNamespace)
            self.storeResults                  (supersetNamespace)

        requiredNamespaces = set()
        requiredNamespaces.update(set(self.__component.namespaces.required       ))
        requiredNamespaces.update(set(self.__carvedoutProgram.namespaces.required))

        for requiredNamespace in requiredNamespaces:
            namespace = self.__sourceFiles.getNamespaceName_from_nameSpacePrintingName(requiredNamespace)
            self.getRequiredNamespaceProperties(requiredNamespace)
            self.storeResults                  (namespace        )

    def printResults(self, namespacesPrintingNames):
        for namespacePrintingName in namespacesPrintingNames:
            namespace = self.__sourceFiles.getNamespaceName_from_nameSpacePrintingName(namespacePrintingName)
    
            self.loadResults              (namespace)
            self.printNamespaceProperties (namespace)
            self.printWrongDependencies   (namespacePrintingName)

    def printNamespaceProperties(self, namespace):
        moduleRedundantSubroutine_wrt_Component        = set()
        moduleRedundantVariables_wrt_Component         = set()
        moduleRedundantTypes_wrt_Component             = set()
        moduleRedundantParameters_wrt_Component        = set()

        moduleRequiredSubroutine_wrt_Component         = set()
        moduleRequiredVariables_wrt_Component          = set()
        moduleRequiredTypes_wrt_Component              = set()
        moduleRequiredParameters_wrt_Component         = set()

        moduleRedundantSubroutine_wrt_CarvedoutProgram = set()
        moduleRedundantVariables_wrt_CarvedoutProgram  = set()
        moduleRedundantTypes_wrt_CarvedoutProgram      = set()
        moduleRedundantParameters_wrt_CarvedoutProgram = set()

        moduleRequiredSubroutine_wrt_CarvedoutProgram  = set()
        moduleRequiredVariables_wrt_CarvedoutProgram   = set()
        moduleRequiredTypes_wrt_CarvedoutProgram       = set()
        moduleRequiredParameters_wrt_CarvedoutProgram  = set()

        moduleRedundantSubroutine_wrt_CarvedoutProgram.update (self.__program.namespaces[namespace].subroutinesDedicatedToComponent       )           
        moduleRedundantSubroutine_wrt_CarvedoutProgram.update (self.__program.namespaces[namespace].deadSubroutines                       )
        moduleRedundantVariables_wrt_CarvedoutProgram.update  (self.__program.namespaces[namespace].variablesDedicatedToComponent         )
        moduleRedundantVariables_wrt_CarvedoutProgram.update  (self.__program.namespaces[namespace].deadVariables                         )
        moduleRedundantParameters_wrt_CarvedoutProgram.update (self.__program.namespaces[namespace].parametersDedicatedToComponent        )
        moduleRedundantParameters_wrt_CarvedoutProgram.update (self.__program.namespaces[namespace].deadParameters                        )
        moduleRedundantTypes_wrt_CarvedoutProgram.update      (self.__program.namespaces[namespace].typesDedicatedToComponent             )
        moduleRedundantTypes_wrt_CarvedoutProgram.update      (self.__program.namespaces[namespace].deadTypes                             )

        moduleRedundantSubroutine_wrt_Component.update        (self.__program.namespaces[namespace].subroutinesDedicatedToCarvedoutProgram)
        moduleRedundantSubroutine_wrt_Component.update        (self.__program.namespaces[namespace].deadSubroutines                       )
        moduleRedundantVariables_wrt_Component.update         (self.__program.namespaces[namespace].variablesDedicatedToCarvedoutProgram  )
        moduleRedundantVariables_wrt_Component.update         (self.__program.namespaces[namespace].deadVariables                         )
        moduleRedundantParameters_wrt_Component.update        (self.__program.namespaces[namespace].parametersDedicatedToCarvedoutProgram )
        moduleRedundantParameters_wrt_Component.update        (self.__program.namespaces[namespace].deadParameters                        )
        moduleRedundantTypes_wrt_Component.update             (self.__program.namespaces[namespace].typesDedicatedToCarvedoutProgram      )
        moduleRedundantTypes_wrt_Component.update             (self.__program.namespaces[namespace].deadTypes                             )

        moduleRequiredSubroutine_wrt_CarvedoutProgram.update  (self.__program.namespaces[namespace].subroutinesDedicatedToCarvedoutProgram)           
        moduleRequiredSubroutine_wrt_CarvedoutProgram.update  (self.__program.namespaces[namespace].sharedSubroutines                     )
        moduleRequiredVariables_wrt_CarvedoutProgram.update   (self.__program.namespaces[namespace].variablesDedicatedToCarvedoutProgram  )
        moduleRequiredVariables_wrt_CarvedoutProgram.update   (self.__program.namespaces[namespace].sharedVariables                       )
        moduleRequiredParameters_wrt_CarvedoutProgram.update  (self.__program.namespaces[namespace].parametersDedicatedToCarvedoutProgram )
        moduleRequiredParameters_wrt_CarvedoutProgram.update  (self.__program.namespaces[namespace].sharedParameters                      )
        moduleRequiredTypes_wrt_CarvedoutProgram.update       (self.__program.namespaces[namespace].typesDedicatedToCarvedoutProgram      )
        moduleRequiredTypes_wrt_CarvedoutProgram.update       (self.__program.namespaces[namespace].sharedTypes                           )
                                                              
        moduleRequiredSubroutine_wrt_Component.update         (self.__program.namespaces[namespace].subroutinesDedicatedToComponent       )
        moduleRequiredSubroutine_wrt_Component.update         (self.__program.namespaces[namespace].sharedSubroutines                     )
        moduleRequiredVariables_wrt_Component.update          (self.__program.namespaces[namespace].variablesDedicatedToComponent         )
        moduleRequiredVariables_wrt_Component.update          (self.__program.namespaces[namespace].sharedVariables                       )
        moduleRequiredParameters_wrt_Component.update         (self.__program.namespaces[namespace].parametersDedicatedToComponent        )
        moduleRequiredParameters_wrt_Component.update         (self.__program.namespaces[namespace].sharedParameters                      )
        moduleRequiredTypes_wrt_Component.update              (self.__program.namespaces[namespace].typesDedicatedToComponent             )  
        moduleRequiredTypes_wrt_Component.update              (self.__program.namespaces[namespace].sharedTypes                           )

        print("\n\n---------------------- Properties of Module << "+ namespace + " >> -----------------------------")
        print("Required parts w.r.t. the Component:")
        print("    Subroutines : ", moduleRequiredSubroutine_wrt_Component)
        print("    Variables   : ", moduleRequiredVariables_wrt_Component )
        print("    Types       : ", moduleRequiredTypes_wrt_Component     )
        print("    Parameters  : ", moduleRequiredParameters_wrt_Component)

        print("\n\n---------------------------------------------------------------")
        print("Required parts w.r.t. the Carvedout Program:")
        print("    Subroutines : ", moduleRequiredSubroutine_wrt_CarvedoutProgram)
        print("    Variables   : ", moduleRequiredVariables_wrt_CarvedoutProgram ) 
        print("    Types       : ", moduleRequiredTypes_wrt_CarvedoutProgram     )
        print("    Parameters  : ", moduleRequiredParameters_wrt_CarvedoutProgram)

        print("\n\n---------------------------------------------------------------")
        print("Shared parts:")
        print("    Subroutines : ", self.__program.namespaces[namespace].sharedSubroutines)
        print("    Variables   : ", self.__program.namespaces[namespace].sharedVariables  )
        print("    Parameters  : ", self.__program.namespaces[namespace].sharedParameters )
        print("    Types       : ", self.__program.namespaces[namespace].sharedTypes      )

        print("\n\n---------------------------------------------------------------")
        print("Redundant parts:")
        print("    Subroutines : ", self.__program.namespaces[namespace].deadSubroutines)
        print("    Variables   : ", self.__program.namespaces[namespace].deadVariables  )
        print("    Parameters  : ", self.__program.namespaces[namespace].deadParameters )
        print("    Types       : ", self.__program.namespaces[namespace].deadTypes      )

        print("\n\n---------------------------------------------------------------")
        print("Redundancies w.r.t. the Component:")
        print("    Subroutines : ", moduleRedundantSubroutine_wrt_Component)
        print("    Variables   : ", moduleRedundantVariables_wrt_Component )
        print("    Types       : ", moduleRedundantTypes_wrt_Component     )
        print("    Parameters  : ", moduleRedundantParameters_wrt_Component)

        print("\n\n---------------------------------------------------------------")
        print("Redundancies w.r.t. the Carvedout Program:")
        print("    Subroutines : ", moduleRedundantSubroutine_wrt_CarvedoutProgram)
        print("    Variables   : ", moduleRedundantVariables_wrt_CarvedoutProgram )
        print("    Types       : ", moduleRedundantTypes_wrt_CarvedoutProgram     )
        print("    Parameters  : ", moduleRedundantParameters_wrt_CarvedoutProgram)

        print("\n\n---------------------------------------------------------------")
        print("Dedicated parts to Component:")
        print("    Subroutines : ", self.__program.namespaces[namespace].subroutinesDedicatedToComponent)
        print("    Variables   : ", self.__program.namespaces[namespace].variablesDedicatedToComponent  )
        print("    Parameters  : ", self.__program.namespaces[namespace].parametersDedicatedToComponent )
        print("    Types       : ", self.__program.namespaces[namespace].typesDedicatedToComponent      )

        print("\n\n---------------------------------------------------------------")
        print("Dedicated parts to Carvedout Program:")
        print("    Subroutines : ", self.__program.namespaces[namespace].subroutinesDedicatedToCarvedoutProgram)
        print("    Variables   : ", self.__program.namespaces[namespace].variablesDedicatedToCarvedoutProgram  )
        print("    Parameters  : ", self.__program.namespaces[namespace].parametersDedicatedToCarvedoutProgram )
        print("    Types       : ", self.__program.namespaces[namespace].typesDedicatedToCarvedoutProgram      )

        print("\n\n---------------------------------------------------------------")
        print("Original LOC :                          ", self.__program.namespaces[namespace].LOC[ORIGINALLOC                      ])
        print("Required LOC w.r.t. Component         : ", self.__program.namespaces[namespace].LOC[REQUIREDLOC_WRT_COMPONENT        ])
        print("Required LOC w.r.t. Carvedout Program : ", self.__program.namespaces[namespace].LOC[REQUIREDLOC_WRT_CARVEDOUTPROGRAM ])
        print("LOC dedicated to Carvedout Program    : ", self.__program.namespaces[namespace].LOC[DEDICATEDLOC_WRT_CARVEDOUTPROGRAM])
        print("LOC dedicated to Component            : ", self.__program.namespaces[namespace].LOC[DEDICATEDLOC_WRT_COMPONENT       ])
        print("Shared LOC                            : ", self.__program.namespaces[namespace].LOC[SHAREDLOC                        ])
        print("Redundant LOC                         : ", self.__program.namespaces[namespace].LOC[DEADLOC                          ])

    def printWrongDependencies(self, namespacesPrintingName ):
        moduleWrongDependencyDetector = ModuleWrongDependencyDetector(self.__sourceFiles, self.__analysisConfig, self.__program)
        moduleWrongDependencyDetector.printResults(namespacesPrintingName )
        
    def loadResults(self, namespace):
        self.deserialize(namespace)

    def storeResults(self, namespace):
        self.serialize(namespace)

    def serialize(self, namespace):
        import pickle
     
        namespaceSharedSubroutinesFilename                      = self.dataFilePath + namespace + self.subroutinesExtension + self.sharedCodesFileExtension                     
        namespaceSharedVariablesFilename                        = self.dataFilePath + namespace + self.variablesExtension   + self.sharedCodesFileExtension                     
        namespaceSharedTypesFilename                            = self.dataFilePath + namespace + self.typesExtension       + self.sharedCodesFileExtension                     
        namespaceSharedParametersFilename                       = self.dataFilePath + namespace + self.parametersExtension  + self.sharedCodesFileExtension                     

        namespaceDedicatedSubroutinesToCarvedoutProgramFilename = self.dataFilePath + namespace + self.subroutinesExtension + self.dedicatedCodesToCarvedoutProgramFileExtension
        namespaceDedicatedVariablesToCarvedoutProgramFilename   = self.dataFilePath + namespace + self.variablesExtension   + self.dedicatedCodesToCarvedoutProgramFileExtension
        namespaceDedicatedTypesToCarvedoutProgramFilename       = self.dataFilePath + namespace + self.typesExtension       + self.dedicatedCodesToCarvedoutProgramFileExtension
        namespaceDedicatedParametersToCarvedoutProgramFilename  = self.dataFilePath + namespace + self.parametersExtension  + self.dedicatedCodesToCarvedoutProgramFileExtension

        namespaceDedicatedSubroutinesToComponentFilename        = self.dataFilePath + namespace + self.subroutinesExtension + self.dedicatedCodesToComponentFileExtension       
        namespaceDedicatedVariablesToComponentFilename          = self.dataFilePath + namespace + self.variablesExtension   + self.dedicatedCodesToComponentFileExtension       
        namespaceDedicatedTypesToComponentFilename              = self.dataFilePath + namespace + self.typesExtension       + self.dedicatedCodesToComponentFileExtension       
        namespaceDedicatedParametersToComponentFilename         = self.dataFilePath + namespace + self.parametersExtension  + self.dedicatedCodesToComponentFileExtension       

        namespaceDeadSubroutinesFilename                        = self.dataFilePath + namespace + self.subroutinesExtension + self.deadCodesFileExtension                       
        namespaceDeadVariablesFilename                          = self.dataFilePath + namespace + self.variablesExtension   + self.deadCodesFileExtension                       
        namespaceDeadTypesFilename                              = self.dataFilePath + namespace + self.typesExtension       + self.deadCodesFileExtension                       
        namespaceDeadParametersFilename                         = self.dataFilePath + namespace + self.parametersExtension  + self.deadCodesFileExtension                       

        namespaceLOCFilename                                    = self.dataFilePath + namespace                             + self.locFileExtension

        with open(namespaceSharedSubroutinesFilename, 'wb') as fp:
             pickle.dump(self.__program.namespaces[namespace].sharedSubroutines, fp)

        with open(namespaceSharedVariablesFilename,   'wb') as fp:
             pickle.dump(self.__program.namespaces[namespace].sharedVariables,   fp)

        with open(namespaceSharedTypesFilename,       'wb') as fp:
             pickle.dump(self.__program.namespaces[namespace].sharedTypes,       fp)

        with open(namespaceSharedParametersFilename,  'wb') as fp:
             pickle.dump(self.__program.namespaces[namespace].sharedParameters,  fp)



        with open(namespaceDedicatedSubroutinesToCarvedoutProgramFilename, 'wb') as fp:
             pickle.dump(self.__program.namespaces[namespace].subroutinesDedicatedToCarvedoutProgram, fp)

        with open(namespaceDedicatedVariablesToCarvedoutProgramFilename,   'wb') as fp:
             pickle.dump(self.__program.namespaces[namespace].variablesDedicatedToCarvedoutProgram,   fp)

        with open(namespaceDedicatedTypesToCarvedoutProgramFilename,       'wb') as fp:
             pickle.dump(self.__program.namespaces[namespace].typesDedicatedToCarvedoutProgram,       fp)

        with open(namespaceDedicatedParametersToCarvedoutProgramFilename,  'wb') as fp:
             pickle.dump(self.__program.namespaces[namespace].parametersDedicatedToCarvedoutProgram,  fp)



        with open(namespaceDedicatedSubroutinesToComponentFilename, 'wb') as fp:
             pickle.dump(self.__program.namespaces[namespace].subroutinesDedicatedToComponent, fp)

        with open(namespaceDedicatedVariablesToComponentFilename,   'wb') as fp:
             pickle.dump(self.__program.namespaces[namespace].variablesDedicatedToComponent,   fp)

        with open(namespaceDedicatedTypesToComponentFilename,        'wb') as fp:
             pickle.dump(self.__program.namespaces[namespace].typesDedicatedToComponent,       fp)

        with open(namespaceDedicatedParametersToComponentFilename,   'wb') as fp:
             pickle.dump(self.__program.namespaces[namespace].parametersDedicatedToComponent,  fp)



        with open(namespaceDeadSubroutinesFilename, 'wb') as fp:
             pickle.dump(self.__program.namespaces[namespace].deadSubroutines, fp)

        with open(namespaceDeadVariablesFilename,   'wb') as fp:
             pickle.dump(self.__program.namespaces[namespace].deadVariables,   fp)

        with open(namespaceDeadTypesFilename,       'wb') as fp:
             pickle.dump(self.__program.namespaces[namespace].deadTypes,       fp)

        with open(namespaceDeadParametersFilename,  'wb') as fp:
             pickle.dump(self.__program.namespaces[namespace].deadParameters,  fp)


        with open(namespaceLOCFilename, 'wb') as fp:
             pickle.dump(self.__program.namespaces[namespace].LOC, fp)


    def deserialize(self, namespace):
        import pickle
        self.__program.namespaces[namespace].sharedSubroutines                      = set()
        self.__program.namespaces[namespace].sharedVariables                        = set()
        self.__program.namespaces[namespace].sharedTypes                            = set()
        self.__program.namespaces[namespace].sharedParameters                       = set()
                                       
        self.__program.namespaces[namespace].subroutinesDedicatedToCarvedoutProgram = set()
        self.__program.namespaces[namespace].variablesDedicatedToCarvedoutProgram   = set()
        self.__program.namespaces[namespace].typesDedicatedToComponent              = set()
        self.__program.namespaces[namespace].parametersDedicatedToCarvedoutProgram  = set()
        
        self.__program.namespaces[namespace].subroutinesDedicatedToComponent        = set()
        self.__program.namespaces[namespace].variablesDedicatedToComponent          = set()
        self.__program.namespaces[namespace].typesDedicatedToComponent              = set()
        self.__program.namespaces[namespace].parametersDedicatedToComponent         = set()
                                       
        self.__program.namespaces[namespace].deadSubroutines                        = set()
        self.__program.namespaces[namespace].deadVariables                          = set()
        self.__program.namespaces[namespace].deadTypes                              = set()
        self.__program.namespaces[namespace].deadParameters                         = set()
                                       
        self.__program.namespaces[namespace].LOCdedicatedToCarvedoutProgram         = 0
        self.__program.namespaces[namespace].LOCDedicatedToComponent                = 0
        self.__program.namespaces[namespace].shareLOC                               = 0
        self.__program.namespaces[namespace].deadLOC                                = 0

        namespaceSharedSubroutinesFilename                      = self.dataFilePath + namespace + self.subroutinesExtension + self.sharedCodesFileExtension                     
        namespaceSharedVariablesFilename                        = self.dataFilePath + namespace + self.variablesExtension   + self.sharedCodesFileExtension                     
        namespaceSharedTypesFilename                            = self.dataFilePath + namespace + self.typesExtension       + self.sharedCodesFileExtension                     
        namespaceSharedParametersFilename                       = self.dataFilePath + namespace + self.parametersExtension  + self.sharedCodesFileExtension                     

        namespaceDedicatedSubroutinesToCarvedoutProgramFilename = self.dataFilePath + namespace + self.subroutinesExtension + self.dedicatedCodesToCarvedoutProgramFileExtension
        namespaceDedicatedVariablesToCarvedoutProgramFilename   = self.dataFilePath + namespace + self.variablesExtension   + self.dedicatedCodesToCarvedoutProgramFileExtension
        namespaceDedicatedTypesToCarvedoutProgramFilename       = self.dataFilePath + namespace + self.typesExtension       + self.dedicatedCodesToCarvedoutProgramFileExtension
        namespaceDedicatedParametersToCarvedoutProgramFilename  = self.dataFilePath + namespace + self.parametersExtension  + self.dedicatedCodesToCarvedoutProgramFileExtension

        namespaceDedicatedSubroutinesToComponentFilename        = self.dataFilePath + namespace + self.subroutinesExtension + self.dedicatedCodesToComponentFileExtension       
        namespaceDedicatedVariablesToComponentFilename          = self.dataFilePath + namespace + self.variablesExtension   + self.dedicatedCodesToComponentFileExtension       
        namespaceDedicatedTypesToComponentFilename              = self.dataFilePath + namespace + self.typesExtension       + self.dedicatedCodesToComponentFileExtension       
        namespaceDedicatedParametersToComponentFilename         = self.dataFilePath + namespace + self.parametersExtension  + self.dedicatedCodesToComponentFileExtension       

        namespaceDeadSubroutinesFilename                        = self.dataFilePath + namespace + self.subroutinesExtension + self.deadCodesFileExtension                       
        namespaceDeadVariablesFilename                          = self.dataFilePath + namespace + self.variablesExtension   + self.deadCodesFileExtension                       
        namespaceDeadTypesFilename                              = self.dataFilePath + namespace + self.typesExtension       + self.deadCodesFileExtension                       
        namespaceDeadParametersFilename                         = self.dataFilePath + namespace + self.parametersExtension  + self.deadCodesFileExtension                       

        namespaceLOCFilename                                    = self.dataFilePath + namespace                             + self.locFileExtension                       


        with open(namespaceSharedSubroutinesFilename, 'rb') as fp:
             self.__program.namespaces[namespace].sharedSubroutines = pickle.load(fp)

        with open(namespaceSharedVariablesFilename,   'rb') as fp:
             self.__program.namespaces[namespace].sharedVariables   = pickle.load(fp)

        with open(namespaceSharedTypesFilename,       'rb') as fp:
             self.__program.namespaces[namespace].sharedTypes       = pickle.load(fp)

        with open(namespaceSharedParametersFilename,  'rb') as fp:
             self.__program.namespaces[namespace].sharedParameters  = pickle.load(fp)


        with open(namespaceDedicatedSubroutinesToCarvedoutProgramFilename, 'rb') as fp:
             self.__program.namespaces[namespace].subroutinesDedicatedToCarvedoutProgram = pickle.load(fp)

        with open(namespaceDedicatedVariablesToCarvedoutProgramFilename,   'rb') as fp:
             self.__program.namespaces[namespace].variablesDedicatedToCarvedoutProgram   = pickle.load(fp)

        with open(namespaceDedicatedTypesToCarvedoutProgramFilename,       'rb') as fp:
             self.__program.namespaces[namespace].typesDedicatedToCarvedoutProgram       = pickle.load(fp)

        with open(namespaceDedicatedParametersToCarvedoutProgramFilename,   'rb') as fp:
             self.__program.namespaces[namespace].parametersDedicatedToCarvedoutProgram  = pickle.load(fp)


        with open(namespaceDedicatedSubroutinesToComponentFilename, 'rb') as fp:
             self.__program.namespaces[namespace].subroutinesDedicatedToComponent = pickle.load(fp)

        with open(namespaceDedicatedVariablesToComponentFilename,   'rb') as fp:
             self.__program.namespaces[namespace].variablesDedicatedToComponent   = pickle.load(fp)

        with open(namespaceDedicatedTypesToComponentFilename,       'rb') as fp:
             self.__program.namespaces[namespace].typesDedicatedToComponent        = pickle.load(fp)

        with open(namespaceDedicatedParametersToComponentFilename,  'rb') as fp:
             self.__program.namespaces[namespace].parametersDedicatedToComponent   = pickle.load(fp)


        with open(namespaceDeadSubroutinesFilename, 'rb') as fp:
             self.__program.namespaces[namespace].deadSubroutines = pickle.load(fp)

        with open(namespaceDeadVariablesFilename,   'rb') as fp:
             self.__program.namespaces[namespace].deadVariables   = pickle.load(fp)

        with open(namespaceDeadTypesFilename,       'rb') as fp:
             self.__program.namespaces[namespace].deadTypes       = pickle.load(fp)

        with open(namespaceDeadParametersFilename,  'rb') as fp:
             self.__program.namespaces[namespace].deadParameters  = pickle.load(fp)


        with open(namespaceLOCFilename, 'rb') as fp:
             self.__program.namespaces[namespace].LOC = pickle.load(fp)



    def getSupersetNamespaceProperties(self, namespace):
        module = self.__sourceFiles.getModule_from_namespace(namespace)
        if module is None:
           printWarning('Module not found: ' + str(namespace), 'ModulePropertiesExtractor')
           return

        self.__module = module
        
        self.__moduleSharedLOC                                  = 0
        self.__moduleSharedSubroutines                          = set()
        self.__moduleSharedGlobalVariables                      = set() 
        self.__moduleSharedParameters                           = set()
        self.__moduleSharedTypes                                = set()

        self.__moduleDedicatedLOC_to_carvedoutProgram           = 0
        self.__moduleSubroutinesDedicatedToCarvedoutProgram     = set()
        self.__moduleGlobalVariablesDedicatedToCarvedoutProgram = set()
        self.__moduleParametersDedicatedToCarvedoutProgram      = set()
        self.__moduleTypesDedicatedToCarvedoutProgram           = set()

        self.__moduleDedicatedLOC_to_component                  = 0
        self.__moduleSubroutinesDedicatedToComponent            = set()
        self.__moduleGlobalVariablesDedicatedToComponent        = set()
        self.__moduleParametersDedicatedToComponent             = set()
        self.__moduleTypesDedicatedToComponent                  = set()

        self.__moduleDeadLOC                                    = 0
        self.__moduleDeadSubroutines                            = set()
        self.__moduleDeadGlobalVariables                        = set()
        self.__moduleDeadParameters                             = set()
        self.__moduleDeadTypes                                  = set()

        self.__moduleOriginalLOC                                = self.__module.originalLOC
        self.getUpdateModuleProperties(namespace)

    def getRequiredNamespaceProperties(self, namespacePrintingName):        
        module = self.__sourceFiles.getModule_from_nameSpacePrintingName(namespacePrintingName)
        if module is None:
           printWarning('Module not found: ' + str(namespacePrintingName), 'ModulePropertiesExtractor')
           return

        self.__module = module

        self.__moduleSharedLOC                                  = 0
        self.__moduleSharedSubroutines                          = set()
        self.__moduleSharedGlobalVariables                      = set() 
        self.__moduleSharedParameters                           = set()
        self.__moduleSharedTypes                                = set()

        self.__moduleDedicatedLOC_to_carvedoutProgram           = 0
        self.__moduleSubroutinesDedicatedToCarvedoutProgram     = set()
        self.__moduleGlobalVariablesDedicatedToCarvedoutProgram = set()
        self.__moduleParametersDedicatedToCarvedoutProgram      = set()
        self.__moduleTypesDedicatedToCarvedoutProgram           = set()

        self.__moduleDedicatedLOC_to_component                  = 0
        self.__moduleSubroutinesDedicatedToComponent            = set()
        self.__moduleGlobalVariablesDedicatedToComponent        = set()
        self.__moduleParametersDedicatedToComponent             = set()
        self.__moduleTypesDedicatedToComponent                  = set()

        self.__moduleDeadLOC                                    = 0
        self.__moduleDeadSubroutines                            = set()
        self.__moduleDeadGlobalVariables                        = set()
        self.__moduleDeadParameters                             = set()
        self.__moduleDeadTypes                                  = set()

        self.__moduleOriginalLOC                                = self.__module.originalLOC
         
        self.getModuleSubroutinesProperties()
        self.getModuleVariablessProperties ()
        self.getModuleParametersProperties ()
        self.getModuleTypessProperties     ()

        if self.__module.getNameSpaceType() != FortranProgramMainUnit and self.__module.getNameSpaceType() != FortranProgramGlobalNameSpace:
            if  self.__moduleSharedLOC != 0:
                self.__moduleSharedLOC += 2
            elif self.__moduleDedicatedLOC_to_carvedoutProgram != 0 and self.__moduleDedicatedLOC_to_component != 0:
                self.__moduleSharedLOC = 2
            elif self.__moduleDedicatedLOC_to_carvedoutProgram != 0:
                self.__moduleDedicatedLOC_to_carvedoutProgram +=2
            elif self.__moduleDedicatedLOC_to_component != 0:
                self.__moduleDedicatedLOC_to_component +=2

            sharedParts                      = len(self.__moduleSharedGlobalVariables)+len(self.__moduleSharedParameters)+len(self.__moduleSharedTypes)
            dedicatedPartsToCarvedoutProgram = len(self.__moduleGlobalVariablesDedicatedToCarvedoutProgram)+len(self.__moduleParametersDedicatedToCarvedoutProgram)+len(self.__moduleTypesDedicatedToCarvedoutProgram)
            dedicatedPartsToComponent        = len(self.__moduleGlobalVariablesDedicatedToComponent)+len(self.__moduleParametersDedicatedToComponent)+len(self.__moduleTypesDedicatedToComponent)

            if  sharedParts > 0 or (dedicatedPartsToCarvedoutProgram > 0 and dedicatedPartsToComponent > 0):
                self.__moduleSharedLOC += 1
            elif dedicatedPartsToCarvedoutProgram > 0:
                self.__moduleDedicatedLOC_to_carvedoutProgram +=1
            elif dedicatedPartsToComponent > 0:
                self.__moduleDedicatedLOC_to_component +=1
            else: 
                self.__moduleDeadLOC += 1

            if self.__moduleDedicatedLOC_to_carvedoutProgram == 0 and self.__moduleDedicatedLOC_to_component == 0 and self.__moduleDeadLOC == 0 and self.__moduleSharedLOC != 0:
                self.__moduleSharedLOC = self.__moduleOriginalLOC
      
        namespace = self.__sourceFiles.getNamespaceName_from_nameSpacePrintingName(namespacePrintingName)
        self.getUpdateModuleProperties(namespace)

    def getUpdateModuleProperties(self, namespace):
        self.__program.namespaces[namespace].sharedSubroutines.update                     (self.__moduleSharedSubroutines                          )
        self.__program.namespaces[namespace].sharedVariables.update                       (self.__moduleSharedGlobalVariables                      ) 
        self.__program.namespaces[namespace].sharedParameters.update                      (self.__moduleSharedParameters                           )
        self.__program.namespaces[namespace].sharedTypes.update                           (self.__moduleSharedTypes                                )
                                  
        self.__program.namespaces[namespace].subroutinesDedicatedToCarvedoutProgram.update(self.__moduleSubroutinesDedicatedToCarvedoutProgram     )
        self.__program.namespaces[namespace].variablesDedicatedToCarvedoutProgram.update  (self.__moduleGlobalVariablesDedicatedToCarvedoutProgram )
        self.__program.namespaces[namespace].parametersDedicatedToCarvedoutProgram.update (self.__moduleParametersDedicatedToCarvedoutProgram      )
        self.__program.namespaces[namespace].typesDedicatedToCarvedoutProgram.update      (self.__moduleTypesDedicatedToCarvedoutProgram           )
                                  
        self.__program.namespaces[namespace].subroutinesDedicatedToComponent.update       (self.__moduleSubroutinesDedicatedToComponent            )
        self.__program.namespaces[namespace].variablesDedicatedToComponent.update         (self.__moduleGlobalVariablesDedicatedToComponent        )
        self.__program.namespaces[namespace].parametersDedicatedToComponent.update        (self.__moduleParametersDedicatedToComponent             )
        self.__program.namespaces[namespace].typesDedicatedToComponent.update             (self.__moduleTypesDedicatedToComponent                  )
                                                                                                  
        self.__program.namespaces[namespace].deadSubroutines.update                       (self.__moduleDeadSubroutines                            )
        self.__program.namespaces[namespace].deadVariables.update                         (self.__moduleDeadGlobalVariables                        )
        self.__program.namespaces[namespace].deadParameters.update                        (self.__moduleDeadParameters                             )
        self.__program.namespaces[namespace].deadTypes.update                             (self.__moduleDeadTypes                                  )                                                                   

        self.__program.namespaces[namespace].LOC[REQUIREDLOC_WRT_COMPONENT] = self.__moduleOriginalLOC - (self.__moduleDedicatedLOC_to_carvedoutProgram + self.__moduleDeadLOC)
        requiredLOC  = 0
        requiredLOC += len(self.__moduleSubroutinesDedicatedToComponent    )
        requiredLOC += len(self.__moduleGlobalVariablesDedicatedToComponent)
        requiredLOC += len(self.__moduleParametersDedicatedToComponent     )
        requiredLOC += len(self.__moduleTypesDedicatedToComponent          ) 
        requiredLOC += self.__moduleSharedLOC
        if requiredLOC == 0: 
            self.__program.namespaces[namespace].LOC[REQUIREDLOC_WRT_COMPONENT] = 0 


        self.__program.namespaces[namespace].LOC[REQUIREDLOC_WRT_CARVEDOUTPROGRAM] = self.__moduleOriginalLOC - (self.__moduleDedicatedLOC_to_component + self.__moduleDeadLOC)
        requiredLOC  = 0
        requiredLOC += len(self.__moduleSubroutinesDedicatedToCarvedoutProgram    )
        requiredLOC += len(self.__moduleGlobalVariablesDedicatedToCarvedoutProgram)
        requiredLOC += len(self.__moduleParametersDedicatedToCarvedoutProgram     )
        requiredLOC += len(self.__moduleTypesDedicatedToCarvedoutProgram          )
        requiredLOC += self.__moduleSharedLOC
        if requiredLOC == 0:
            self.__program.namespaces[namespace].LOC[REQUIREDLOC_WRT_CARVEDOUTPROGRAM] = 0 
          

        self.__program.namespaces[namespace].LOC[DEDICATEDLOC_WRT_CARVEDOUTPROGRAM] = self.__moduleDedicatedLOC_to_carvedoutProgram
        self.__program.namespaces[namespace].LOC[DEDICATEDLOC_WRT_COMPONENT       ] = self.__moduleDedicatedLOC_to_component
        self.__program.namespaces[namespace].LOC[SHAREDLOC                        ] = self.__moduleSharedLOC
        self.__program.namespaces[namespace].LOC[DEADLOC                          ] = self.__moduleDeadLOC                          
        self.__program.namespaces[namespace].LOC[ORIGINALLOC                      ] = self.__moduleOriginalLOC

    def getModuleSubroutinesProperties(self): 
        componentGlobalSubroutines        = self.__component.calledSubroutinesFullNames
        carvedoutProgramGlobalSubroutines = self.__carvedoutProgram.calledSubroutinesFullNames

        moduleGlobalSubroutines           = [("__"+subroutine.getModuleName().upper()+"_MOD_"+subroutineName.upper(), subroutine) for subroutineName, subroutine  in self.__module.getSubroutines().items()]

        for moduleSubroutineName, moduleSubroutine in moduleGlobalSubroutines:
            moduleSubroutineSimpleName = moduleSubroutine.getSimpleName().lower()
            if len(carvedoutProgramGlobalSubroutines) > 0 and len(componentGlobalSubroutines) >0 and moduleSubroutineName in carvedoutProgramGlobalSubroutines and moduleSubroutineName in componentGlobalSubroutines:
                self.__moduleSharedSubroutines.add(moduleSubroutineSimpleName)
                self.__moduleSharedLOC = self.__moduleSharedLOC + moduleSubroutine.originalLOC
            elif len(carvedoutProgramGlobalSubroutines) >0 and moduleSubroutineName in carvedoutProgramGlobalSubroutines:
                self.__moduleSubroutinesDedicatedToCarvedoutProgram.add(moduleSubroutineSimpleName)
                self.__moduleDedicatedLOC_to_carvedoutProgram = self.__moduleDedicatedLOC_to_carvedoutProgram + moduleSubroutine.originalLOC
            elif len(componentGlobalSubroutines) > 0 and moduleSubroutineName in componentGlobalSubroutines:
                self.__moduleSubroutinesDedicatedToComponent.add(moduleSubroutineSimpleName)
                self.__moduleDedicatedLOC_to_component = self.__moduleDedicatedLOC_to_component + moduleSubroutine.originalLOC
            else:
                self.__moduleDeadSubroutines.add(moduleSubroutineSimpleName)
                self.__moduleDeadLOC = self.__moduleDeadLOC + moduleSubroutine.originalLOC

        if self.__module.getNameSpaceType() != FortranProgramMainUnit and self.__module.getNameSpaceType() != FortranProgramGlobalNameSpace and len(moduleGlobalSubroutines) > 0:
            if len(self.__moduleSharedSubroutines) > 0 or (len(self.__moduleSubroutinesDedicatedToCarvedoutProgram) > 0 and len(self.__moduleSubroutinesDedicatedToComponent) > 0):
                 self.__moduleSharedLOC += 1
            elif len(self.__moduleSubroutinesDedicatedToCarvedoutProgram) > 0:
                self.__moduleDedicatedLOC_to_carvedoutProgram +=1
            elif len(self.__moduleSubroutinesDedicatedToComponent) > 0:
                self.__moduleDedicatedLOC_to_component +=1
            else:
                self.__moduleDeadLOC += 1

    def getModuleVariablessProperties(self):
        componentGlobalVariables        = [variableFullName.upper()                                                       for variableFullName       in self.__component.globalVariablesFullNames       ]
        carvedoutProgramGlobalVariables = [variableFullName.upper()                                                       for variableFullName       in self.__carvedoutProgram.globalVariablesFullNames]

        moduleGlobalVariables           = [("__"+variable.getModuleName().upper()+"_MOD_"+variableName.upper(), variable) for variableName, variable in self.getModuleGlobalVariables().items()]

        for variableName, variable in moduleGlobalVariables:
            if len(carvedoutProgramGlobalVariables) > 0 and len(componentGlobalVariables) > 0 and variableName in carvedoutProgramGlobalVariables and variableName in componentGlobalVariables:
                self.__moduleSharedGlobalVariables.add(variable.getName().lower())
                self.__moduleSharedLOC = self.__moduleSharedLOC + variable.requiredLOC
            elif len(carvedoutProgramGlobalVariables) > 0 and variableName in carvedoutProgramGlobalVariables:
                self.__moduleGlobalVariablesDedicatedToCarvedoutProgram.add(variable.getName().lower())
                self.__moduleDedicatedLOC_to_carvedoutProgram = self.__moduleDedicatedLOC_to_carvedoutProgram + variable.requiredLOC
            elif len(componentGlobalVariables) > 0 and variableName in componentGlobalVariables:
                self.__moduleGlobalVariablesDedicatedToComponent.add(variable.getName().lower())
                self.__moduleDedicatedLOC_to_component = self.__moduleDedicatedLOC_to_component + variable.requiredLOC
            else: 
                self.__moduleDeadGlobalVariables.add(variable.getName().lower())
                self.__moduleDeadLOC = self.__moduleDeadLOC + variable.requiredLOC

    def getModuleParametersProperties(self):
        componentGlobalParameters        = [parameterFullName.upper()                                                         for parameterFullName        in self.__component.globalParametersFullNames       ]
        carvedoutProgramGlobalParameters = [parameterFullName.upper()                                                         for parameterFullName        in self.__carvedoutProgram.globalParametersFullNames]
        moduleGlobalParameters           = [("__"+parameter.getModuleName().upper()+"_MOD_"+parameterName.upper(), parameter) for parameterName, parameter in self.__module.getParameters().items()   ]

        for parameterName, parameter in moduleGlobalParameters:
            if len(carvedoutProgramGlobalParameters) > 0 and len(componentGlobalParameters) > 0 and parameterName in carvedoutProgramGlobalParameters and parameterName in componentGlobalParameters:
                self.__moduleSharedParameters.add(parameter.getName().lower())
                self.__moduleSharedLOC = self.__moduleSharedLOC + parameter.requiredLOC
            elif len(carvedoutProgramGlobalParameters) > 0 and parameterName in carvedoutProgramGlobalParameters:
                self.__moduleParametersDedicatedToCarvedoutProgram.add(parameter.getName().lower())
                self.__moduleDedicatedLOC_to_carvedoutProgram = self.__moduleDedicatedLOC_to_carvedoutProgram + parameter.requiredLOC
            elif len(componentGlobalParameters) > 0 and parameterName in componentGlobalParameters:
                self.__moduleParametersDedicatedToComponent.add(parameter.getName().lower())
                self.__moduleDedicatedLOC_to_component = self.__moduleDedicatedLOC_to_component + parameter.requiredLOC
            else: 
                self.__moduleDeadParameters.add(parameter.getName().lower())
                self.__moduleDeadLOC = self.__moduleDeadLOC + parameter.requiredLOC
 
    def getModuleTypessProperties(self):
        module_globalDerivedTypesDeclarations = [(typE.getName(),typE) for typE in self.findDeclaredTypes(self.__module.getStatementsBeforeContains())]

        for typeName,typE in module_globalDerivedTypesDeclarations:              
            typeFullName = "__" + self.__module.getName().lower() + "_MOD_" + typeName.lower()
            if typeFullName in self.__carvedoutProgram.globalDerivedTypesFullNames and typeFullName in self.__component.globalDerivedTypesFullNames:
                self.__moduleSharedTypes.add(typeName)
                self.__moduleSharedLOC = self.__moduleSharedLOC + typE.requiredLOC
            elif typeFullName in self.__carvedoutProgram.globalDerivedTypesFullNames:
                self.__moduleTypesDedicatedToCarvedoutProgram.add(typeName)
                self.__moduleDedicatedLOC_to_carvedoutProgram = self.__moduleDedicatedLOC_to_carvedoutProgram + typE.requiredLOC
            elif typeFullName in self.__component.globalDerivedTypesFullNames:
                self.__moduleTypesDedicatedToComponent.add(typeName)
                self.__moduleDedicatedLOC_to_component = self.__moduleDedicatedLOC_to_component + typE.requiredLOC
            else:
                self.__moduleDeadTypes.add(typeName)
                self.__moduleDeadLOC = self.__moduleDeadLOC + typE.requiredLOC

    def getModuleGlobalVariables(self):
        variables = dict()

        self.__module.clearVariablesCache()
        candidateVariables = self.__module.getVariables()
        for variableName, variable in candidateVariables.items():
            if not variable.isParameter():
                variables[variableName] = variable

        return variables

    def findDeclaredTypes(self, statements):
        useRegEx     = re.compile(r'^USE[\s\:]+(?P<modulename>[a-z0-9_]+)\s*(\,.*)?$', re.IGNORECASE);
        useOnlyRegRE = re.compile(r"^USE\s+[a-z0-9_]+\s*\,\s*ONLY\s*\:\s*(?P<statements1>[\w]+\s*)(?P<statements2>(\s*\,\s*[\w]+\s*){0,})", re.IGNORECASE)

        abstractTypes = self.__sourceFiles.getConfig().getAbstractTypesInAnalysis()
        for abstractType, subtype in abstractTypes.items():
            if isinstance(subtype, tuple) and len(subtype) == 2:
                        if self.__sourceFiles.existsModule(subtype[1]):
                            abstractTypes[abstractType] = subtype[0]
                        else:
                            abstractTypes[abstractType] = subtype[1]

        typeFinder = TypeFinder(abstractTypes)
        for i, statement, j in statements:
            useRegExMatch = useRegEx.match(statement)
            if useRegExMatch is None:
                typeFinder.parseStatement(i, statement, j, self.__module)

        collection        = typeFinder.getResult()
        typeDict, typeSet = collection.getResult()
        return(list(typeSet))
