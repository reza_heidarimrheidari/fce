import os

'''
@edited by: Mohammad Reza Heidari 
'''

# Config file for Fortran Component Extraction (FCE) Toolbox
# Can be placed in Fortran Component Extraction (FCE) Toolbox's root directory or in a subdirectory called config

#--------------------------------------------------------------------------
# Directory where serialized call trees are stored for quicker analysis
# OPTIONAL: If omitted call trees won't be cached
CACHE_DIR = os.path.dirname(os.path.realpath(__file__)) + '/cache'

#--------------------------------------------------------------------------
# Locations of the assembler files 
# Directories are searched in the order of this list
# Subdirectories are included automatically 
# REQUIRED
ASSEMBLER_DIRS = ["./"]

#--------------------------------------------------------------------------
# Locations of the original source files
# same as above
# REQUIRED
SOURCE_DIRS = ["./"] 

#----------------------- The Path for Saving Output Data ------------------
DATA_DIR = "./data/"

#--------------------------------------------------------------------------
# Are the files in SOURCE_DIRS already preprocessed?
# OPTIONAL, default: False
SOURCE_FILES_PREPROCESSED = False 

#--------------------------------------------------------------------------
# dict of all modules that are not defined in a filename with correspondig name (module_name.f90)
# Format 'module_name':'actual_file_name'
# OPTIONAL
SPECIAL_MODULE_FILES = {"test":"allmodules"}

#--------------------------------------------------------------------------
# Modules to be excluded completely from the analysis
# OPTIONAL
EXCLUDE_MODULES = []

#--------------------------------------------------------------------------
#EXCLUDE_SUBROUTINES = EXCLUDE_UNKNOWN_SUBROUTINES + EXCLUDE_LAPACK_SUBROUTINES + EXCLUDE_DUMMYARGUEMENT_SUBROUTINES
EXCLUDE_SUBROUTINES = []

#--------------------------------------------------------------------------
# Modules from which global variables shall not be listed when running -a globals or -a all
# OPTIONAL
IGNORE_GLOBALS_FROM_MODULES = EXCLUDE_MODULES + [] 

#--------------------------------------------------------------------------
# Types whose components shall not be listed when running -a ... 
# OPTIONAL
IGNORE_DERIVED_TYPES = []

#--------------------------------------------------------------------------
# List of types of which all components are considered to be used as soon as a variable of that type appears
# OPTIONAL
ALWAYS_FULL_TYPES = {}

#--------------------------------------------------------------------------
# dict of subtypes that are chosen as the one and only implementation of an abstract type. 
# FCG handles variables of a given abstract type as if the type were the given subtype.
# Format: 'abstract_type':('subtype_module','subtype')
# If the module with analyzed subroutine has a dependency to subtype_module anyway, you can leave it away:
# 'abstract_type':'subtype'
# OPTIONAL
ABSTRACT_TYPE_IMPLEMENTATIONS = {}

#------------- Fortran Files Containing Global Subroutines ----------------
GLOBALSUBROUTINES_FILES = {"example":"example.f90","mainsub":"mainsub.f90","main0sub":"main0sub.f90"}

#--------------------------- Entrypoints of Components --------------------
PROGRAM_MAINUNIT_NAME = "example"

#--------------------------------------------------------------------------
COMPONENT_ENTRYPOINTS = [("component",{"Sub1"})]

#--------------------------------------------------------------------------
modules_in_Redundancy_and_Dependency_Analysis = []
#modules_in_Redundancy_and_Dependency_Analysis.extend(['datatype' ])
#modules_in_Redundancy_and_Dependency_Analysis.extend(['module4'])
modules_in_Redundancy_and_Dependency_Analysis.extend(['module3'  ])
#modules_in_Redundancy_and_Dependency_Analysis.extend(['module2'  ])
#modules_in_Redundancy_and_Dependency_Analysis.extend(['component'])

globalSubroutines_SimpleNames_in_Redundancy_and_Dependency_Analysis = []
#globalSubroutines_SimpleNames_in_Redundancy_and_Dependency_Analysis.extend(['main0sub'])
#globalSubroutines_SimpleNames_in_Redundancy_and_Dependency_Analysis.extend(['mainsub' ])

programMainUnitName_in_Redundancy_and_Dependency_Analysis = []
#programMainUnitName_in_Redundancy_and_Dependency_Analysis = ['example']

modules_printinNames           = []
globalSubroutines_printinNames = []
programMainUnit_printingName   = []

[modules_printinNames.append          ('(Module) '           + module              ) for module               in modules_in_Redundancy_and_Dependency_Analysis                      ]
[globalSubroutines_printinNames.append('(Global subroutine) '+ subroutineSimpleName) for subroutineSimpleName in globalSubroutines_SimpleNames_in_Redundancy_and_Dependency_Analysis]
[programMainUnit_printingName.append  ('(Program) '          + programName         ) for programName          in programMainUnitName_in_Redundancy_and_Dependency_Analysis          ]

CFG_TARGETMODULE_FOR_REDUNDANCY_AND_DEPENDENCY_ANALYSIS = modules_printinNames + globalSubroutines_printinNames + programMainUnit_printingName

