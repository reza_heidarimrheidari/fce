# coding=utf8
from supertypes import UsePrinter, UseTraversalPassenger
from assertions import assertTypeAll, assertType
from source import SourceFiles, SubroutineFullName, Module
from usetraversal import UseTraversal
from printout import printLines

class UseCollector(UseTraversalPassenger):
 
    def __init__(self):
        self.reset()
         
    def reset(self):      
        self.__moduleNames = set()
         
    def getResult(self):
        return self.__moduleNames
 
    def parseStatement(self, i, statement, j, module):  # @UnusedVariable
        assertType(module, 'module', Module)
         
        self.__moduleNames.add(module.getName())
        

class UsedModuleNamePrinter(UsePrinter):        
    def __init__(self, sourceFiles, ndg, programSettings):
        assertType(sourceFiles, 'sourceFiles', SourceFiles)
        self.__ndg             = ndg
        self.__programSettings = programSettings

        super(UsedModuleNamePrinter, self).__init__(sourceFiles)

    def getcarvedoutProgramExtractionEnabled(self):
        return(self.__carvedoutProgramExtractionEnabled)

    def printUses(self, rootSubroutine, carvedoutProgramExtractionEnabled, globalNamespaceSubprograms):
        assertType(rootSubroutine, 'rootSubroutine', SubroutineFullName)
         
        if carvedoutProgramExtractionEnabled and rootSubroutine._name in self.__programSettings.getComponentEntrypointsFullNamesString():
            return

        useTraversal = UseTraversal(self._sourceFiles, self.__ndg, self.__programSettings, globalNamespaceSubprograms, carvedoutProgramExtractionEnabled = carvedoutProgramExtractionEnabled)
        useCollector = UseCollector()
        useTraversal.addPassenger(useCollector)
        useTraversal.parseModules(rootSubroutine)
        modules = useCollector.getResult()
        modules.add(rootSubroutine.getModuleName())
        return(useTraversal.getInterfaces(), useTraversal.getTypes().getResult()[0], useTraversal.getRequiredExcludedModules())
        
    def _printModules(self, modules):
        printLines(sorted(modules))
        

class UsedFileNamePrinter(UsedModuleNamePrinter):
    
    def __init__(self, sourceFiles, excludeModules = []):
        assertType(sourceFiles, 'sourceFiles', SourceFiles)
        assertTypeAll(excludeModules, 'excludeModules', str)
        
        super(UsedFileNamePrinter, self).__init__(sourceFiles, excludeModules)
    
    def _printModules(self, modules):
        
        files = set()
        for moduleName in modules:
            sourceFile = self._sourceFiles.findModuleFile(moduleName)
            if sourceFile is not None:
                path = self._sourceFiles.getRelativePath(sourceFile)
                files.add(path)
        
        printLines(sorted(files))
