# Fortran Component Extraction (FCE) Toolbox

Fortran Component Extraction (FCE) Toolbox is a set of static source code analysis tools for Fortran. 

Its main purpose is to extract the source code of an arbitrary component from a Fortran program and identify the shared source code and variables between the component and the program.

Fortran Component Extraction (FCE) Toolbox is written in Python and is able to analyze Fortran90+ source files. It is based on  FortranCallGraph by Christian Hovy.

The documentation is so far poor but should be added in the future.

**Contents:** [Quick Start Guide](#quick-start-guide)

## Quick Start Guide

### 1. Clone this repo

### 2. Fill out the configuration file [fceconfig.py](fceconfig.py):

### 3. Create assembler files

Compile your Fortran application with [gfortran](https://gcc.gnu.org/fortran) and the options `-S -g -O0` or `-save-temps -g -O0` to generate assembler files.

### 4. Run the following tools in the same directory that your Fortyran program resides:
    (a) `./fortanalysis.py`   to do the static program analysis.
    (b) `./fortextraction.py` to extract an overestimated component.
    (c) `./fortcleaning.py`   to clean the dead codes from the component.
    (d) `./fortdep.py`        to see which parts of any arbitrary Fortran file belongs to the component. 
       

## License

[GNU General Public License v3.0](LICENSE)
