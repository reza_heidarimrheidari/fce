#coding=utf8

import re
from assertions import assertType, assertTypeAll, REGEX_TYPE
from supertypes import CallGraphAnalyzer, CallGraphBuilder
from source import SourceFiles, Module, VariableReference, SourceFile, FortranModuleNameSpaceType, InnerSubroutineName, ComponentImaginaryMasterEntrypointFullName
from callgraph import CallGraph
from trackvariable import VariableTracker, VariableTrackerSettings
from usetraversal import UseTraversal
from typefinder import TypeCollection
from printout import printLine, printWarning
from dependency import ContainerDependenciesDetector

class GlobalParameterTracker(CallGraphAnalyzer):

    __routineWarnings = set()
    __moduleWarnings = set()

    def __init__(self, sourceFiles, ndg, programSettings, clearCache, interfaces = None, types = None, callGraphBuilder = None):
        assertType(sourceFiles,          'sourceFiles', SourceFiles)
        assertType(interfaces,            'interfaces', dict, True)
        assertType(types,                      'types', TypeCollection, True)
        assertType(callGraphBuilder,'callGraphBuilder', CallGraphBuilder, True)
        
        super(GlobalParameterTracker, self).__init__()

        self.__sourceFiles      = sourceFiles
        self.__ndg              = ndg
        self.__programSettings  = programSettings
        self.__clearCache       = clearCache
        self.__interfaces       = interfaces
        self.__types            = types
        self.__callGraphBuilder = callGraphBuilder

        self.__settings                          = VariableTrackerSettings()
        self.__settings.excludeModules           = self.__programSettings.getExcludeModulesInAnalysis()
        self.__settings.ignoreGlobalsFromModules = self.__programSettings.getIgnoreGlobalsFromModulessInAnalysis()
        self.__settings.ignoredTypes             = self.__programSettings.getIgnoreDerivedTypesInAnalysis()
        self.__settings.fullTypes                = self.__programSettings.getAlwaysFullTypesInAnalysis()
        self.__settings.abstractTypes            = self.__programSettings.getAbstractTypesInAnalysis()
        self.__settings.ignoreSubroutinesRegex   = None
        self.__settings.minimalOutput            = False
        self.__settings.pointersOnly             = False
        assertType   (self.__settings,                          'settings', VariableTrackerSettings)
        assertTypeAll(self.__settings.excludeModules,           'settings.excludeModules',           str)
        assertTypeAll(self.__settings.ignoreGlobalsFromModules, 'settings.ignoreGlobalsFromModules', str)
        assertTypeAll(self.__settings.ignoredTypes,             'settings.ignoredTypes',             str)
        assertType   (self.__settings.abstractTypes,            'settings.abstractTypes',            dict)
        assertType   (self.__settings.ignoreSubroutinesRegex,   'settings.ignoreSubroutinesRegex',   REGEX_TYPE, True)
        assertType   (self.__settings.minimalOutput,            'settings.minimalOutput',            bool)
        assertType   (self.__settings.pointersOnly,             'settings.pointersOnly',             bool)

        self.__usedParameterLists                = dict()
        self.__parameterTracker                  = None
        self.__callGraph                         = None      
        self.__globalNamespaceSubprograms        = None
        self.__carvedoutProgramExtractionEnabled = None
        
        GlobalParameterTracker.__routineWarnings = set()
        self.visitedExcludedModules              = set()    

    def getVisitedExcludedModules(self):
        return(self.visitedExcludedModules)

    def getGlobalParameters(self, callGraph, globalNamespaceSubprograms, carvedoutProgramExtractionEnabled):
        '''Analyzes the given Callgraph. Finds all global variables.'''
        assertType(callGraph, 'callGraph', CallGraph)

        self.visitedExcludedModules              = set()
        self.__globalNamespaceSubprograms        = globalNamespaceSubprograms
        self.__carvedoutProgramExtractionEnabled = carvedoutProgramExtractionEnabled
        parameterReferences                      = self.trackGlobalParameters(callGraph)
        return [parameterReference.getLevel0Variable() for parameterReference in parameterReferences]

    def analyzeCallgraph(self, callGraph):
        '''Analyzes the given Callgraph. Finds all references to global variables.'''
        assertType(callGraph, 'callGraph', CallGraph)
            
        variableReferences = self.trackGlobalVariables(callGraph)
        
        if not self.__settings.minimalOutput:
            for variableReference in variableReferences:
                if not self.__settings.pointersOnly or variableReference.isPointer():
                    printLine(variableReference)
        else:
            for variableReference in variableReferences:
                if not self.__settings.pointersOnly or variableReference.isPointer():
                    if variableReference.getDeclaredIn() is not None:
                        declaredIn = ' {' + variableReference.getDeclaredIn().getName() + '}'
                    else:
                        declaredIn = ''
                        
                    printLine(variableReference.getExpression() + declaredIn)
                    
    def trackGlobalParameters(self, callGraph):
        assertType(callGraph, 'callGraph', CallGraph)
        
        self.__callGraph = callGraph
        
        if self.__interfaces is None or self.__types is None:
            useTraversal = UseTraversal(self.__sourceFiles, self.__ndg, self.__programSettings, self.__globalNamespaceSubprograms)

            if self.__carvedoutProgramExtractionEnabled:
                useTraversal.parseModules(callGraph.getRoot())
            else:
                subroutineName                             = callGraph.getRoot()
                componentImaginaryMasterEntrypointFullName = ComponentImaginaryMasterEntrypointFullName
                if subroutineName._name.lower() == componentImaginaryMasterEntrypointFullName.lower():            
                    for componentEntrypointFullName in callGraph.getCallees(subroutineName):
                         useTraversal.parseModules(componentEntrypointFullName)
    
            self.__interfaces = useTraversal.getInterfaces()
            self.__types = useTraversal.getTypes()
        
        self.__variableTracker = VariableTracker(self.__sourceFiles, self.__programSettings, self.__carvedoutProgramExtractionEnabled, self.__clearCache, self.__interfaces, self.__types, self.__callGraphBuilder)
        variableReferences = self.__analyzeSubroutines(callGraph.getAllSubroutineNames());
        variableReferences = VariableReference.sort(variableReferences)
        self.__variableTracker = None
        self.__callGraph = None
        
        return variableReferences      
                
    def __analyzeSubroutines(self, subroutineNames):
        variableReferences = set();
        for subroutineName in subroutineNames:
            variableReferences.update(self.__analyzeSubroutine(subroutineName));
        return variableReferences;


    def __analyzeSubroutine(self, subroutineName):
        if self.__settings.matchIgnoreSubroutineRegex(subroutineName) or (subroutineName.getModuleName().lower in self.__settings.excludeModules):
            self.visitedExcludedModules.add(subroutineName.getNamespacePrintingName())
            return set()
 
        subroutine = self.__findSubroutine(subroutineName)
        moduleName          = subroutineName.getModuleName()
        subroutineSimpleName=subroutineName.getSimpleName()
        nameSpaceType       =subroutineName.getNameSpaceType()

        if isinstance(subroutineName, InnerSubroutineName):
            isInnerSubroutine        = True
            hostSubroutineSimpleName = subroutineName.getHostName().getSimpleName() 
        else:
            isInnerSubroutine        = False
            hostSubroutineSimpleName = None

        variables = self.__getModuleParameters(moduleName, subroutineSimpleName, nameSpaceType, isInnerSubroutine, hostSubroutineSimpleName)
       
        allUsedParameters = self.__getAllUsedParameters(subroutineName)
        variables.update(allUsedParameters)

                                                                  
#Reza comment: I think this is not enough to check if there is a local variable with the same name.
#The symbol could be used to define a inner-subroutine, local derived type, local variable, or local parameter.
#So we should check if the symbol has been used for any new declaraion or not.
        for variableName in list(variables.keys()): #list() needed for Python3   
            if subroutine.hasVariable(variableName):                                                                 
                del variables[variableName]
                                                                                                             
        callGraph = self.__callGraph.extractSubgraph(subroutineName)
        normalVariables = set()
        typeVariables = set()
        for variable in variables.values():
            if variable.isParameter():
                if variable.hasDerivedType() and variable.getDerivedTypeName() not in self.__settings.fullTypes:
                    typeVariables.add(variable)
                else:
                    normalVariables.add(variable)                                                                                                                                                                                
        self.__variableTracker.clearOutAssignments()
        typeVariableReferences = set(self.__variableTracker.trackVariables(typeVariables, callGraph))
        funtionResultOriginalReferences = []
        outVarAssignments = []
        for aliasVar, originalReference in self.__variableTracker.getOutAssignments():
            if aliasVar.isFunctionResult():
                funtionResultOriginalReferences.append(originalReference)
            elif aliasVar.isOutArgument():
                outVarAssignments.append((aliasVar, originalReference))
        if funtionResultOriginalReferences:
            typeVariableReferences.update(self.__trackFunctionResult(subroutineName, funtionResultOriginalReferences))
        if outVarAssignments:
            typeVariableReferences.update(self.__trackOutVariables(subroutineName, outVarAssignments))
        
        normalVariableReferences = set(self.__trackVariables(normalVariables, subroutineName))

        references = set()
        for reference in normalVariableReferences | typeVariableReferences:
            variable = reference.getLevel0Variable()
            if variable.isAlias():
                reference.setLevel0Variable(variable.getOriginal())
            references.add(reference)

        return references
    
    def __trackFunctionResult(self, functionName, originalReferences):
        variables = [originalReferences[0].getLevelNVariable().getAlias(functionName.getSimpleName())] # TODO type-bound-procedures???
        for interface in self.__interfaces.values():
            if functionName.getSimpleName() in interface:
                variables.append(originalReferences[0].getLevelNVariable().getAlias(interface.getName()))
        tracker = VariableTracker(self.__sourceFiles, self.__programSettings, self.__carvedoutProgramExtractionEnabled, self.__clearCache, self.__interfaces, self.__types, self.__callGraphBuilder)
        variableReferences = set()
                            
        for callerName in self.__callGraph.getCallers(functionName):
            callGraph = self.__callGraph.extractSubgraph(callerName)
            functionReferences = set(tracker.trackVariables(variables, callGraph))
            if not functionReferences:
                for originalReference in originalReferences:
                    functionReferences.update(self.__analyzeCallingSubroutineForTypeBoundFunctionResult(callerName, functionName, originalReference))
            for functionReference in functionReferences:
                for originalReference in originalReferences:
                    variableReference = functionReference.cleanCopy()
                    variableReference.setLevel0Variable(originalReference.getLevel0Variable(), originalReference.getMembers())
                    variableReferences.add(variableReference)
            
        return variableReferences
    
    def __trackOutVariables(self, calleeName, assignments):
        variableReferences = set()
        if assignments:
            for callerName in self.__callGraph.getCallers(calleeName):
                variableReferences.update(self.__analyzeCallingSubroutineForOutVars(callerName, calleeName, assignments))
                
        return variableReferences
    
    def __analyzeCallingSubroutineForTypeBoundFunctionResult(self, callerName, calleeName, originalReference):
        calleeNameAlternatives = []
        for typE in self.__types:
            if typE.containsSubroutine(calleeName.getSimpleName()):
                calleeNameAlternatives.append(typE.getSubroutineAlias(calleeName.getSimpleName()).lower())
        
        caller = self.__findSubroutine(callerName)
        callee = self.__findSubroutine(calleeName)
        callGraph = self.__callGraph.extractSubgraph(callerName) 
        
        variableReferences = set()
        if caller is not None and callee is not None:
            for lineNumber, statement, _ in caller.getStatements():
                tracker = VariableTracker(self.__sourceFiles, self.__programSettings, self.__carvedoutProgramExtractionEnabled, self.__clearCache, self.__interfaces, self.__types, self.__callGraphBuilder)
                for calleeNameAlternative in calleeNameAlternatives:
                    assignmentRegEx = re.compile(r'^(?P<alias>[a-z0-9_]+)(\(.*\))?\s*\=\>?\s*.*%' + calleeNameAlternative + '\s*\((?P<arguments>.*)\).*$', re.IGNORECASE);
                    assignmentRegExMatch = assignmentRegEx.match(statement)
                    if assignmentRegExMatch is not None:
                        assemblerLineNumber = lineNumber - caller.getSourceFile().getPreprocessorOffset(lineNumber)
                        if calleeName in self.__callGraph.findNextCalleesFromLine(callerName, assemblerLineNumber):
                            alias = assignmentRegExMatch.group('alias')
                            variableReferences.update(tracker.trackAssignment(alias, originalReference, callGraph, lineNumber))
                variableReferences.update(self.__trackOutVariables(callerName, tracker.getOutAssignments()))
            
        return variableReferences
    
    def __analyzeCallingSubroutineForOutVars(self, callerName, calleeName, assignments):
        calleeNameAlternatives = [calleeName.getSimpleName().lower()]
        for interface in self.__interfaces.values():
            if calleeName.getSimpleName() in interface:
                calleeNameAlternatives.append(interface.getName().lower())
        for typE in self.__types:
            if typE.containsSubroutine(calleeName.getSimpleName()):
                calleeNameAlternatives.append(typE.getSubroutineAlias(calleeName.getSimpleName()).lower())
        
        caller = self.__findSubroutine(callerName)
        callee = self.__findSubroutine(calleeName)
        callGraph = self.__callGraph.extractSubgraph(callerName) 

        variableReferences = set()
        if caller is not None and callee is not None:
            for lineNumber, statement, _ in caller.getStatements():
                tracker = VariableTracker(self.__sourceFiles, self.__programSettings, self.__carvedoutProgramExtractionEnabled, self.__clearCache, self.__interfaces, self.__types, self.__callGraphBuilder)
                for calleeNameAlternative in calleeNameAlternatives:
                    procedureRegEx = re.compile(r'^.*(?P<prefix>[^a-z0-9_]+)' + calleeNameAlternative + '\s*\((?P<arguments>.*)\).*$', re.IGNORECASE);
                    procedureRegExMatch = procedureRegEx.match(statement)
                    if procedureRegExMatch is not None:
                        isCall = True
                        if calleeNameAlternative != calleeName.getSimpleName():
                            assemblerLineNumber = lineNumber - caller.getSourceFile().getPreprocessorOffset(lineNumber)
                            isCall = calleeName in self.__callGraph.findNextCalleesFromLine(callerName, assemblerLineNumber)
                        if isCall:
                            arguments = procedureRegExMatch.group('arguments')
                            arguments = SourceFile.removeUnimportantParentheses(arguments)
                            arguments = arguments.split(',')
                            typeBound = procedureRegExMatch.group('prefix')[-1:] == '%'
                            for alias, originalReference in assignments:
                                aliasPosition = callee.getArgumentPosition(alias)
                                if typeBound:
                                    aliasPosition -= 1
                                if aliasPosition >= 0 and aliasPosition < len(arguments):
                                    argument = arguments[aliasPosition]
                                    variableReferences.update(tracker.trackAssignment(argument, originalReference, callGraph, lineNumber))
                            break
                variableReferences.update(self.__trackOutVariables(callerName, tracker.getOutAssignments()))
            
        return variableReferences
        
    def __trackVariables(self, variables, subroutineName):

        variableReferences = [];
        for variable in variables:
            variableReferences += self.__trackVariable(variable, subroutineName);
        
        return variableReferences;
    
    def __trackVariable(self, variable, subroutineName):
        variableName = variable.getName()
#        accessRegEx = re.compile(r'^(.*[^a-z0-9_])?' + variableName + r'([^a-z0-9_].*)?', re.IGNORECASE);
        accessRegEx = re.compile('^.*'+'(?<![0-9a-z_])'+variableName+'(?![0-9a-z_])'+'.*', re.IGNORECASE)
        tracker = VariableTracker(self.__sourceFiles, self.__programSettings, self.__carvedoutProgramExtractionEnabled, self.__clearCache, self.__interfaces, self.__types, self.__callGraphBuilder)
        
        variableReferences = [];
        subroutine = self.__findSubroutine(subroutineName);
        if subroutine is not None:
            for lineNumber, statement, _ in subroutine.getStatements():
                if accessRegEx.match(statement) is not None:
                    variableReference = VariableReference(variableName, subroutineName, lineNumber, variable)
                    if variable.hasDerivedType() and variable.getDerivedTypeName() in self.__settings.fullTypes:
                        variableReferences += list(tracker.createReferencesForFullTypeVariable(variableReference, subroutineName, lineNumber))
                    else:
                        variableReferences.append(variableReference)
                        
                
        return variableReferences
    
    def __getModuleParameters(self, moduleName, subroutineName, nameSpaceType, isInnerSubroutine, hostSubroutineSimpleName):
        moduleName = moduleName.lower()
        if moduleName in self.__settings.ignoreGlobalsFromModules or moduleName.lower() in self.__settings.excludeModules:
            namespacePrintingName = Module.getNamespacePrintingName_from_moduleName(moduleName)
            self.visitedExcludedModules.add(namespacePrintingName)
            return dict()

        module = self.__findModule(moduleName, subroutineName, nameSpaceType, isInnerSubroutine, hostSubroutineSimpleName)

        if module is None:
            return dict()

        allUsedParameters_in_moduleGlobalScope = self.__getAllUsedParameters_in_moduleGlobalScope(module)

        if self.__settings.ignoreSubroutinesRegex is not None:
            moduleParameters = dict()
            for name, par in module.getParameters_and_usedParametersInParametersDeclaration(allUsedParameters_in_moduleGlobalScope).items():
                if not self.__settings.matchIgnoreSubroutineRegex(name):
                    moduleParameters[name] = par
            return moduleParameters
        else:
            return module.getParameters_and_usedParametersInParametersDeclaration(allUsedParameters_in_moduleGlobalScope)

    def __getAllUsedParameters_in_moduleGlobalScope(self, module):
        allUsedParameters_in_moduleGlobalScope = list(list())

        if module is None:
            return(allUsedParameters_in_moduleGlobalScope)

        if module.getName() in self.__settings.ignoreGlobalsFromModules or module.getName().lower() in self.__settings.excludeModules:
            return(allUsedParameters_in_moduleGlobalScope)
        
        moduleDependenciesDetector        = ContainerDependenciesDetector(self.__programSettings, module, None, self.__sourceFiles)
        dependencies_in_moduleGlobalScope = moduleDependenciesDetector.getDependencies_in_moduleGlobalScope()

        for _, parts in dependencies_in_moduleGlobalScope.items():
            usedModuleName = parts.moduleName

            if usedModuleName in self.__settings.ignoreGlobalsFromModules or usedModuleName.lower() in self.__settings.excludeModules:
                namespacePrintingName = Module.getNamespacePrintingName_from_moduleName(usedModuleName)
                self.visitedExcludedModules.add(namespacePrintingName)
                continue

            usedModule = self.__findModule(usedModuleName, None, FortranModuleNameSpaceType, False, None)
            if usedModule is None:
                #ToDo En error must be raised!
                continue

            globalParameters_in_usedModule = usedModule.getParameters()

            for usedEntity in parts.entities:
                if usedEntity.upper().find("OPERATOR") > -1:
                    #ToDo suppor OPERATOR alaising later                                                                                                                                                                      
                    continue

                if usedEntity.find('=>') > -1:
                    names        = usedEntity.split('=>')
                    aliasName    = names[0].strip()
                    originalName = names[1].strip()
                else:
                    aliasName    = usedEntity
                    originalName = aliasName

                for parameterName, parameter in globalParameters_in_usedModule.items():
                    if parameterName.upper() == originalName.upper():
                       allUsedParameters_in_moduleGlobalScope.append([aliasName, parameter])
        return(allUsedParameters_in_moduleGlobalScope)    

    def __getAllUsedParameters(self, subroutineName):
        usedParameters = dict()
       
        container = self.__findSubroutine(subroutineName)
        while container is not None:
            usedParameters.update(self.__getUsedParametersOfSubroutineContainer(container))
            container = container.getContainer()
            
        return usedParameters
    
    def __getUsedParametersOfSubroutineContainer(self, container):
        name = container.getName()
        if name not in self.__usedParameterLists:
            self.__usedParameterLists[name] = self.__findUsedParametersInSubroutineContainer(container)
                
        return self.__usedParameterLists[name]
    
    def __findUsedParametersInSubroutineContainer(self, container):
        moduleDependenciesDetector        = ContainerDependenciesDetector(self.__programSettings, container, None, self.__sourceFiles)
        dependencies_in_moduleGlobalScope = moduleDependenciesDetector.getDependencies_in_moduleGlobalScope()

        usedParameters = dict()
        for statement, parts in dependencies_in_moduleGlobalScope.items():
            moduleName = parts.moduleName
            if moduleName not in self.__settings.ignoreGlobalsFromModules:
                moduleVariables = self.__getModuleParameters(moduleName, None, FortranModuleNameSpaceType, False, None)
                importList      = parts.entities
                for imported in importList:
                    names = imported.split('=>')
                    names = [n.strip() for n in names]
                    alias = names[0]
                    name = names[len(names) - 1]
                    name = name.lower()
                    if name in moduleVariables:
                        variable = moduleVariables[name]
                        if alias != name:
                            variable = variable.getAlias(alias)
                        usedParameters[alias] = variable
        return usedParameters
    
    def __findUsedParametersInStatements(self, statements):
        useAllRegEx = re.compile(r'^USE[\s\:]+(?P<modulename>[a-z0-9_]+)\s*(\,\s*)?$', re.IGNORECASE)
        useOnlyRegEx = re.compile(r'^USE[\s\:]+(?P<modulename>[a-z0-9_]+)\s*\,\s*ONLY\s*\:\s*(?P<importlist>.*)$', re.IGNORECASE)

        usedParameters = dict()
        for _, statement, _ in statements:
                                 
            useOnlyRegExMatch = useOnlyRegEx.match(statement)
            if useOnlyRegExMatch is not None:
                moduleName = useOnlyRegExMatch.group('modulename')
                                 
                if moduleName not in self.__settings.ignoreGlobalsFromModules:
                    moduleVariables = self.__getModuleParameters(moduleName, None, FortranModuleNameSpaceType, False, None)
                    importList = useOnlyRegExMatch.group('importlist').split(',')
                    importList = [i.strip() for i in importList]
                    for imported in importList:
                        names = imported.split('=>')
                        names = [n.strip() for n in names]
                        alias = names[0]
                        name = names[len(names) - 1]
                        name = name.lower()
                        if name in moduleVariables:
                            variable = moduleVariables[name]
                            if alias != name:
                                variable = variable.getAlias(alias)
                            usedParameters[alias] = variable
            else:
                useAllRegExMatch = useAllRegEx.match(statement)
                                 
                if useAllRegExMatch is not None:
                    moduleName = useAllRegExMatch.group('modulename')
                    if moduleName not in self.__settings.ignoreGlobalsFromModules:
                        usedParameters.update(self.__getModuleParameters(moduleName, None, FortranModuleNameSpaceType, False, None))

        return usedParameters


    def __findSubroutine(self, subroutineName):
        subroutine = self.__sourceFiles.findSubroutine(subroutineName);
        if subroutine is None and subroutineName not in GlobalParameterTracker.__routineWarnings:
            GlobalParameterTracker.__routineWarnings.add(subroutineName)
            printWarning('Routine not found: ' + str(subroutineName), 'GlobalParameterTracker')
            
        return subroutine

    def __findModule(self, moduleName, subroutineName, nameSpaceType, isInnerSubroutine, hostSubroutineSimpleName):
        module = self.__sourceFiles.findModule(moduleName, subroutineName, nameSpaceType, isInnerSubroutine, hostSubroutineSimpleName);

        if module is None and moduleName not in GlobalParameterTracker.__moduleWarnings:
            GlobalParameterTracker.__moduleWarnings.add(moduleName)
            printWarning('Module not found: ' + str(moduleName), 'GlobalParameterTracker')

        return module
