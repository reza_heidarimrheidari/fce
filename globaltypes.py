# coding=utf8
'''
@author: Mohammad Reza Heidari
'''
import re
from assertions import assertType, assertTypeAll
from source import UseStatementParts, SubroutineFullName, SourceFiles, Module
from interfaces import InterfaceFinder
from typefinder import TypeFinder
from printout import printWarning
from source import Module, SourceFiles, ComponentImaginaryMasterEntrypointNameSpace
from supertypes import UsePrinter, UseTraversalPassenger
from usetraversal import UseTraversal
from useprinter import UseCollector
from collections import defaultdict
from printout import printLines, printErrorAndExit
from lister import SubroutineListingCallGraphPrinter
from callgraph import CallGraph
from supertypes import CallGraphPrinter
from trackvariable import VariableTrackerSettings
from dependency import ContainerDependenciesDetector
import copy

class RequiredGlobalDerivedTypesDetector():
    def __init__(self, program, programSubcomponent, sourceFiles):
        self.__program             = program
        self.__programSubcomponent = programSubcomponent
        self.__sourceFiles         = sourceFiles
        self.__module              = None
        self.visitedExcludedModules= set()

    def getVisitedExcludedModules(self):
        return(self.visitedExcludedModules)

    def getResults(self):
        requiredGlobalDerivedTypes = set()  
        self.visitedExcludedModules= set()
        for moduleName in self.__programSubcomponent.namespaces.superSet: 
            if moduleName == ComponentImaginaryMasterEntrypointNameSpace:
                continue 

            if moduleName in self.__program.getExcludeModulesInAnalysis():
                namespacePrintingName = Module.getNamespacePrintingName_from_moduleName(moduleName)
                self.visitedExcludedModules.add(namespacePrintingName)
                continue 

            requiredGlobalDerivedTypes.update(self.findRequiredGlobalDerivedTypesPerModule(moduleName))

        return requiredGlobalDerivedTypes

    def findRequiredGlobalDerivedTypesPerModule(self, moduleName):
        globalDerivedTypes_usedInModule_resolvedDeclarations = []

        if moduleName in self.__program.getExcludeModulesInAnalysis():
            namespacePrintingName = Module.getNamespacePrintingName_from_moduleName(moduleName)
            self.visitedExcludedModules.add(namespacePrintingName)
            return(globalDerivedTypes_usedInModule_resolvedDeclarations)

        self.__module = self.__sourceFiles.getModule(moduleName) 
        if self.__module is None:
            printWarning('Module not found1: ' + str(moduleName), 'RequiredGlobalDerivedTypesDetector')
            return(globalDerivedTypes_usedInModule_resolvedDeclarations)
         
        derivedTypes_usedInSubroutines_missingDeclarations, derivedTypes_usedInSubroutines_declaredInExternalModules = self.getSubroutinesRequiredTypes()

        globalDerivedTypes_usedInModule_missingDeclarations  = derivedTypes_usedInSubroutines_missingDeclarations
        globalDerivedTypes_usedInModule_resolvedDeclarations = derivedTypes_usedInSubroutines_declaredInExternalModules
        derivedTypes_declaredIn_ModuleGlobalScope            = [typE.getName().lower() for typE in self.findDeclaredTypes(self.__module.getStatementsBeforeContains())]

        for var in self.__programSubcomponent.globalVariables:
            if var.getModuleName().lower() == moduleName.lower():
                if var.hasDerivedType():
                    globalDerivedTypes_usedInModule_missingDeclarations.add(var.getDerivedTypeName().lower())
         
        self.__moduleDependenciesDetector = ContainerDependenciesDetector(self.__program, self.__module, self.__programSubcomponent, self.__sourceFiles)
        allUseStatements = self.__moduleDependenciesDetector.getDependencies_wrt_Subcomponent()

        for typE in copy.deepcopy(globalDerivedTypes_usedInModule_missingDeclarations):
            if typE in derivedTypes_declaredIn_ModuleGlobalScope:
                globalDerivedTypes_usedInModule_resolvedDeclarations.add("__" + moduleName.lower() + "_MOD_" + typE) 
                globalDerivedTypes_usedInModule_missingDeclarations.remove(typE)
            else: 
                for statement, parts in allUseStatements.items():
                    usedModuleEntities = [] 
                    for usedModuleEntity in parts.entities:
                        if usedModuleEntity.find('=>') > -1:
                            names    = usedModuleEntity.split('=>')
                            alias    = names[0].strip()
                            original = names[1].strip()
                            entity   = original
                        else:
                            entity = usedModuleEntity
                        usedModuleEntities.append(entity.lower())

                    if typE in usedModuleEntities:
                        globalDerivedTypes_usedInModule_resolvedDeclarations.add("__" + parts.moduleName.lower() + "_MOD_" + typE)
                        globalDerivedTypes_usedInModule_missingDeclarations.remove(typE)
                        break
        
        if len(globalDerivedTypes_usedInModule_missingDeclarations) > 0: 
            printWarning("Missing type declarations in module "+ str(moduleName)+" Please make sure your Fortran program already compiles successfuly.", 'RequiredGlobalDerivedTypesDetector')

        return(globalDerivedTypes_usedInModule_resolvedDeclarations)

    def getSubroutinesRequiredTypes(self):
        subroutinesRequiredDerivedTypesDeclarationsInModuleGlobalScope = set()
        subroutinesRequiredDerivedTypesDeclarationsFromExternalModules = set()       
        calledSubroutines = [subroutine._name for subroutine in self.__programSubcomponent.subroutines]

        for subroutineName in self.__module.getSubroutines():
            moduleName = self.__module.getName()
            subroutineFullName = ""
            if SubroutineFullName.validParts(moduleName, subroutineName):
                subroutineFullName = SubroutineFullName.fromParts(moduleName, subroutineName)._name
            else:
                printErrorAndExit(5, 'Invalid Module and/or Subroutine name: ' + moduleName + ', ' + subroutineName)
 
            if subroutineFullName in calledSubroutines:
                subroutineRequiredDerivedTypesDeclarationsInModuleGlobalScope, subroutineRequiredDerivedTypesDeclarationsFromExternalModules = self.getSubroutineRequiredTypes(self.__module.getSubroutine(subroutineName))
                subroutinesRequiredDerivedTypesDeclarationsInModuleGlobalScope.update(subroutineRequiredDerivedTypesDeclarationsInModuleGlobalScope)
                subroutinesRequiredDerivedTypesDeclarationsFromExternalModules.update(subroutineRequiredDerivedTypesDeclarationsFromExternalModules) 

        return(subroutinesRequiredDerivedTypesDeclarationsInModuleGlobalScope, subroutinesRequiredDerivedTypesDeclarationsFromExternalModules)

          
    def getSubroutineRequiredTypes(self, subroutine):
        subroutineRequiredDerivedTypes = []
        subroutineVariables            = subroutine.getVariables()
        for var in subroutineVariables:
            if var.hasDerivedType():
                subroutineRequiredDerivedTypes.append(var.getDerivedTypeName().lower())
          
        subroutineDeclaredTypes = [typE.getName().lower() for typE in self.findDeclaredTypes(subroutine.getStatements())]
        subroutineMissingDerivedTypesDeclarations = set(subroutineRequiredDerivedTypes)
        for typE in copy.deepcopy(subroutineMissingDerivedTypesDeclarations):
            if typE in subroutineDeclaredTypes:
                subroutineMissingDerivedTypesDeclarations.remove(typE)

        self.__subroutineDependenciesDetector = ContainerDependenciesDetector(self.__program, subroutine, self.__programSubcomponent, self.__sourceFiles)
        allUseStatements = self.__subroutineDependenciesDetector.getDependencies_wrt_Subcomponent()
     
        subroutineRequiredDerivedTypesDeclarationsFromExternalModules = set()
        for typE in copy.deepcopy(subroutineMissingDerivedTypesDeclarations):
            for statement, parts in allUseStatements.items():
                usedModuleEntities = []
                for usedModuleEntity in parts.entities:
                    if usedModuleEntity.find('=>') > -1:
                        names    = usedModuleEntity.split('=>')
                        alias    = names[0].strip()
                        original = names[1].strip()
                        entity   = original
                    else:
                        entity = usedModuleEntity
                    usedModuleEntities.append(entity.lower())

                if typE in usedModuleEntities:
                    subroutineRequiredDerivedTypesDeclarationsFromExternalModules.add("__" + parts.moduleName.lower() + "_MOD_" + typE)
                    subroutineMissingDerivedTypesDeclarations.remove(typE)
                    break
        
        return (subroutineMissingDerivedTypesDeclarations, subroutineRequiredDerivedTypesDeclarationsFromExternalModules)

    def getEntities_declaredInModuleGlobalScope(self, moduleName):
        entities_declaredInModuleGlobalScope = []

        if moduleName in self.__program.getExcludeModulesInAnalysis():
            namespacePrintingName = Module.getNamespacePrintingName_from_moduleName(moduleName)
            self.visitedExcludedModules.add(namespacePrintingName)
            return(entities_declaredInModuleGlobalScope)

        module = self.__sourceFiles.getModule(moduleName) 
        if module is None:
            printWarning('Module not found2: ' + str(moduleName), 'RequiredGlobalDerivedTypesDetector')
            return(entities_declaredInModuleGlobalScope)

        subroutines_declaredInModuleGlobalScope  = [subroutineSimpleName for subroutineSimpleName in module.getSubroutines().keys()]
        derivedTypes_declaredInModuleGlobalScope = self.getDerivedTypes_declaredInModuleGlobalScope(moduleName)
        variables_declaredInModuleGlobalScope    = [variableName for variableName in module.getVariables()]
        parameters_declaredInModuleGlobalScope   = [parameterName for parameterName in module.getParameters()]

        entities_declaredInModuleGlobalScope = subroutines_declaredInModuleGlobalScope + derivedTypes_declaredInModuleGlobalScope + variables_declaredInModuleGlobalScope + parameters_declaredInModuleGlobalScope
        return(entities_declaredInModuleGlobalScope)

    def getDerivedTypes_declaredInModuleGlobalScope(self, moduleName):
        derivedTypes_declaredIn_moduleGlobalScope = []

        if moduleName in self.__program.getExcludeModulesInAnalysis():
            namespacePrintingName = Module.getNamespacePrintingName_from_moduleName(moduleName)
            self.visitedExcludedModules.add(namespacePrintingName)
            return(derivedTypes_declaredIn_moduleGlobalScope)

        module = self.__sourceFiles.getModule(moduleName) 
        if module is None:
            printWarning('Module not found3: ' + str(moduleName), 'RequiredGlobalDerivedTypesDetector')
            return(derivedTypes_declaredIn_moduleGlobalScope)

        derivedTypes_declaredIn_moduleGlobalScope = [typE.getName().lower() for typE in self.findDeclaredTypes(module.getStatementsBeforeContains())]
        return(derivedTypes_declaredIn_moduleGlobalScope)

    def findDeclaredTypes(self, statements):
        useRegEx     = re.compile(r'^USE[\s\:]+(?P<modulename>[a-z0-9_]+)\s*(\,.*)?$', re.IGNORECASE);
        useOnlyRegRE = re.compile(r"^USE\s+[a-z0-9_]+\s*\,\s*ONLY\s*\:\s*(?P<statements1>[\w]+\s*)(?P<statements2>(\s*\,\s*[\w]+\s*){0,})", re.IGNORECASE)

        abstractTypes = self.__sourceFiles.getConfig().getAbstractTypesInAnalysis()
        for abstractType, subtype in abstractTypes.items():
            if isinstance(subtype, tuple) and len(subtype) == 2:
                        if self.__sourceFiles.existsModule(subtype[1]):
                            abstractTypes[abstractType] = subtype[0]
                        else:
                            abstractTypes[abstractType] = subtype[1]

        typeFinder = TypeFinder(abstractTypes)
        for i, statement, j in statements:
            useRegExMatch = useRegEx.match(statement)
            if useRegExMatch is None:
                typeFinder.parseStatement(i, statement, j, self.__module)

        collection        = typeFinder.getResult()
        typeDict, typeSet = collection.getResult()
        return(list(typeSet))
