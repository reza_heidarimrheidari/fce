#!/usr/bin/python

'''
@author: Mohammad Reza Heidari
'''

from staticprogramanalysis import StaticProgramAnalysis

def fortCleaning():
    staticProgramAnalysis = StaticProgramAnalysis()
    staticProgramAnalysis.printResults_MinimalSubcomponents()

if __name__ == "__main__":
    fortCleaning()
