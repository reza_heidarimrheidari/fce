# coding=utf8
'''
@author: Mohammad Reza Heidari                                                                                                                                                                                                                
'''

from ndg                       import NDG
from source                    import SourceFiles, ORIGINALCODESNAME, COMPONENTCODESNAME, CARVEDOUTPROGRAMCODESNAME, SHAREDCODESNAME
from trackvariable             import VariableTrackerSettings
from useprinter                import UsedModuleNamePrinter
from assembler                 import GNUx86AssemblerCallGraphBuilder
from programsettings           import ProgramSettings
from collections               import defaultdict
from subcomponents             import SubcomponentsExtraction
from modulepropertiesextractor import ModulesPropertiesExtractor,        \
                                      REQUIREDLOC_WRT_CARVEDOUTPROGRAM,  \
                                      REQUIREDLOC_WRT_COMPONENT,         \
                                      DEDICATEDLOC_WRT_CARVEDOUTPROGRAM, \
                                      DEDICATEDLOC_WRT_COMPONENT,        \
                                      SHAREDLOC, DEADLOC, ORIGINALLOC 

class ProgramNamespaceProperties:
      def __init__(self):
          self.subroutinesDedicatedToCarvedoutProgram = set()
          self.variablesDedicatedToCarvedoutProgram   = set()
          self.parametersDedicatedToCarvedoutProgram  = set()
          self.typesDedicatedToCarvedoutProgram       = set()
          self.LOCDedicatedToCarvedoutProgram         = 0
                                                      
          self.subroutinesDedicatedToComponent        = set()
          self.variablesDedicatedToComponent          = set()
          self.parametersDedicatedToComponent         = set()
          self.typesDedicatedToComponent              = set()
          self.LOCDedicatedToComponent                = 0
                                                      
          self.sharedSubroutines                      = set()
          self.sharedVariables                        = set()
          self.sharedParameters                       = set()
          self.sharedTypes                            = set()
          self.shareLOC                               = 0 
                                                      
          self.deadSubroutines                        = set()  
          self.deadVariables                          = set()
          self.deadParameters                         = set()
          self.deadTypes                              = set()
          self.deadLOC                                = 0

class ProgramSubcomponentNamespaces:
      def __init__(self):
          self.superSet               = list()
          self.required               = list()
          self.excluded_from_superSet = set()
          self.excluded_from_required = set()

          self.supersetSorted         = list()

class ProgramSubcomponentLOC:
      def __init__(self):
          self.superSet               = 0
          self.required               = 0
          self.excluded_from_superSet = 0
          self.excluded_from_required = 0

class ProgramSubcomponent:
      def __init__(self, name):
          self.name                                = name

          self.subroutines                         = []
          self.globalVariables                     = []
          self.globalParameters                    = []

          self.calledSubroutinesFullNames          = set()
          self.globalVariablesFullNames            = set()
          self.globalParametersFullNames           = set()
          self.globalDerivedTypesFullNames         = set()
          self.globalInterfacesFullNames           = set()

          self.supersetGlobalVariablesFullNames    = set()
          self.supersetGlobalDerivedTypesFullNames = set()
          self.entrypointsFullNames                = set()

          self.LOC                                 = ProgramSubcomponentLOC() 
          self.namespaces                          = ProgramSubcomponentNamespaces()

          self.callGraph                           = None
          self.isCarveoutProgram                   = None 

class ProgramOriginalNamespaceProperties:
      def __init__(self):
          self.sharedSubroutines                      = set() 
          self.sharedVariables                        = set()
          self.sharedParameters                       = set()
          self.sharedTypes                            = set()
          self.sharedInterfaces                       = set()
                                                        
          self.subroutinesDedicatedToCarvedoutProgram = set()
          self.variablesDedicatedToCarvedoutProgram   = set()
          self.parametersDedicatedToCarvedoutProgram  = set()
          self.typesDedicatedToCarvedoutProgram       = set()
          self.interfacesDedicatedToCarvedoutProgram  = set()
                                                        
          self.subroutinesDedicatedToComponent        = set()
          self.variablesDedicatedToComponent          = set()
          self.parametersDedicatedToComponent         = set()
          self.typesDedicatedToComponent              = set()
          self.interfacesDedicatedToComponent         = set()
      
          self.deadSubroutines                        = set()
          self.deadVariables                          = set()
          self.deadParameters                         = set()
          self.deadTypes                              = set()
          self.deadInterfaces                         = set()
  
          self.LOC                                    = [0] * len([ORIGINALLOC, 
                                                                   REQUIREDLOC_WRT_CARVEDOUTPROGRAM, 
                                                                   REQUIREDLOC_WRT_COMPONENT, 
                                                                   DEDICATEDLOC_WRT_CARVEDOUTPROGRAM, 
                                                                   DEDICATEDLOC_WRT_COMPONENT, 
                                                                   SHAREDLOC, DEADLOC])

class Program:
      def __init__(self):
          self.original         = ProgramSubcomponent (ORIGINALCODESNAME                 )
          self.component        = ProgramSubcomponent (COMPONENTCODESNAME                )
          self.carvedoutProgram = ProgramSubcomponent (CARVEDOUTPROGRAMCODESNAME         )
          self.sharedCodes      = ProgramSubcomponent (SHAREDCODESNAME                   )
          self.namespaces       = defaultdict         (ProgramOriginalNamespaceProperties)

class StaticProgramAnalysis:
      def __init__(self):
          self.__program                                       = Program()
          self.__analysisConfig                                = ProgramSettings()

          self.__program.component.entrypointsFullNames        = self.__analysisConfig.getComponentEntrypointsFullNames()
          self.__program.carvedoutProgram.entrypointsFullNames = self.__analysisConfig.getMainProgramUnitFullname()

          self.__program.component.isCarveoutProgram           = False
          self.__program.carvedoutProgram.isCarveoutProgram    = True

          self.__sourceFiles                                   = SourceFiles(self.__analysisConfig)
          self.__ndg                                           = NDG()
          self.__graphBuilder                                  = GNUx86AssemblerCallGraphBuilder(self.__ndg, self.__analysisConfig)
          self.__printer                                       = UsedModuleNamePrinter(self.__sourceFiles, self.__ndg, self.__analysisConfig)
         
          self.__trackerSettings                               = VariableTrackerSettings()
          self.__trackerSettings.excludeModules                = self.__analysisConfig.getExcludeModulesInAnalysis()
          self.__trackerSettings.ignoreGlobalsFromModules      = self.__analysisConfig.getIgnoreGlobalsFromModulessInAnalysis()
          self.__trackerSettings.ignoredTypes                  = self.__analysisConfig.getIgnoreDerivedTypesInAnalysis()
          self.__trackerSettings.fullTypes                     = self.__analysisConfig.getAlwaysFullTypesInAnalysis()
          self.__trackerSettings.abstractTypes                 = self.__analysisConfig.getAbstractTypesInAnalysis()
          self.__trackerSettings.ignoreSubroutinesRegex        = None
          self.__trackerSettings.minimalOutput                 = False
          self.__trackerSettings.pointersOnly                  = False

          self.__clearCache                                    = False

          self.__modulesPropertiesDetector                     = ModulesPropertiesExtractor(self.__sourceFiles, self.__analysisConfig, self.__program)

          print("Component Entrypoints :", self.__analysisConfig.getComponentEntrypointsFullNamesString())
          print("\n\n\n")

      def getSubcomponents(self):
          subcomponentsExtraction = SubcomponentsExtraction(self.__sourceFiles, self.__ndg, self.__analysisConfig, self.__graphBuilder, self.__program)
          subcomponentsExtraction.getResults()

      def printResults_SubcomponentsOverview(self):
          subcomponentsExtraction = SubcomponentsExtraction(self.__sourceFiles, self.__ndg, self.__analysisConfig, self.__graphBuilder, self.__program)
          subcomponentsExtraction.printResultsOverview()

      def printResults_SupersetSubcomponents(self):
          subcomponentsExtraction = SubcomponentsExtraction(self.__sourceFiles, self.__ndg, self.__analysisConfig, self.__graphBuilder, self.__program)
          subcomponentsExtraction.printSupersetSubcomponents()

      def printResults_MinimalSubcomponents(self):
          subcomponentsExtraction = SubcomponentsExtraction(self.__sourceFiles, self.__ndg, self.__analysisConfig, self.__graphBuilder, self.__program)
          subcomponentsExtraction.printMinimalSubcomponents()
          
#          subcomponentsLOCExtraction = SubcomponentsLOCExtraction(self.__sourceFiles, self.__analysisConfig, self.__graphBuilder, self.__program)
#          subcomponentsLOCExtraction.printRequiredResults()

      def printModuleProperties(self):
          subcomponentsExtraction = SubcomponentsExtraction(self.__sourceFiles, self.__ndg, self.__analysisConfig, self.__graphBuilder, self.__program)
          subcomponentsExtraction.loadResults()

          moduleNames                = self.__analysisConfig.getTargetModule_for_redundancyAndDependencyAnalysis()     
          modulesPropertiesExtractor = ModulesPropertiesExtractor (self.__sourceFiles, self.__analysisConfig, self.__program)
          modulesPropertiesExtractor.printResults(moduleNames)
