#!/usr/bin/python

'''
@author: Mohammad Reza Heidari
'''

from staticprogramanalysis import StaticProgramAnalysis

def fortExtraxtion():
    staticProgramAnalysis = StaticProgramAnalysis()
    staticProgramAnalysis.printResults_SupersetSubcomponents()

if __name__ == "__main__":
    fortExtraxtion()
