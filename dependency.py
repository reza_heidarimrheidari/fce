# coding=utf8
'''                                                                                                                                                                                                                                           
@author: Mohammad Reza Heidari                                                                                                                                                                                                                
'''

import re
from assertions  import assertType
from collections import defaultdict
from typefinder  import TypeFinder
from printout    import printWarning
from source      import UseStatementParts, SubroutineFullName, SourceFiles

class ContainerDependenciesDetector():
    def __init__(self, program, container, programSubcomponent, sourceFiles):
        self.__program             = program
        self.__container           = container
        self.__sourceFiles         = sourceFiles
        self.__programSubcomponent = programSubcomponent

    def getDependencies_in_moduleGlobalScope(self):
        dependencies_in_containerGlobalScope = defaultdict(UseStatementParts)

        if self.__container is None:
            return(dependencies_in_containerGlobalScope)

        containerGlobalScope_useStatements_with_ONLYEntities, containerGlobalScope_useStatements_lackingEntities = self.__container.getUseStatementsEntities()             

        explicitDependencies = defaultdict(UseStatementParts)
        explicitDependencies.update(containerGlobalScope_useStatements_with_ONLYEntities)

        allUseStatements_lackingEntities = defaultdict(UseStatementParts)
        allUseStatements_lackingEntities.update(containerGlobalScope_useStatements_lackingEntities)
        implicitDependencies = self.findImplicitDependencies_createdBy_UseStatementsLackingEntities(allUseStatements_lackingEntities)

        dependencies_in_containerGlobalScope.update (explicitDependencies)
        dependencies_in_containerGlobalScope.update (implicitDependencies)
        return(dependencies_in_containerGlobalScope)

    def getDependencies_wrt_Subcomponent(self):
        dependencies_in_Container_wrt_Subcomponent = defaultdict(UseStatementParts)

        if self.__container is None or self.__programSubcomponent is None:
            return(dependencies_in_Container_wrt_Subcomponent)

        containerGlobalScope_useStatements_with_ONLYEntities, containerGlobalScope_useStatements_lackingEntities = self.__container.getUseStatementsEntities()                 

        explicitDependencies = defaultdict(UseStatementParts)
        explicitDependencies.update(containerGlobalScope_useStatements_with_ONLYEntities)

        allUseStatements_lackingEntities = defaultdict(UseStatementParts)
        allUseStatements_lackingEntities.update(containerGlobalScope_useStatements_lackingEntities)
        implicitDependencies = self.findImplicitDependencies_createdBy_UseStatementsLackingEntities(allUseStatements_lackingEntities)
       
        calledSubroutinesNames = [subroutinesFullName.upper() for subroutinesFullName in self.__programSubcomponent.calledSubroutinesFullNames]

        dependencies_in_localSubroutines = defaultdict(UseStatementParts)
        for subroutineName, subroutine in self.__container.getSubroutines().items():
            if subroutine.getName()._name.upper() in calledSubroutinesNames:
                subroutineDependency = ContainerDependenciesDetector(self.__program, subroutine, self.__programSubcomponent, self.__sourceFiles)
                dependencies_in_localSubroutines.update(subroutineDependency.getDependencies_wrt_Subcomponent())         

        dependencies_in_Container_wrt_Subcomponent.update (explicitDependencies)
        dependencies_in_Container_wrt_Subcomponent.update (implicitDependencies)
        dependencies_in_Container_wrt_Subcomponent.update (dependencies_in_localSubroutines)
        return(dependencies_in_Container_wrt_Subcomponent)

    def findImplicitDependencies_createdBy_UseStatementsLackingEntities(self, useStatements_lackingEntities):        
        implicitDependencies = defaultdict(UseStatementParts)
           
        for statement, parts in useStatements_lackingEntities.items():
            implicitDependencies[statement].moduleName = parts.moduleName
            
            internalSymbols_declaredInModuleGlobalScope   = self.getInternalSymbols_declaredInModuleGlobalScope  (parts.moduleName)
            dependencies_inModuleGlobalScope = self.getDependencies_inModuleGlobalScope(parts.moduleName)

            implicitDependencies[statement].entities = internalSymbols_declaredInModuleGlobalScope
            implicitDependencies.update(dependencies_inModuleGlobalScope)
        return(implicitDependencies)

    def getInternalSymbols_declaredInModuleGlobalScope(self, moduleName):
        internalSymbols_declaredInModuleGlobalScope = []

        if moduleName in self.__program.getExcludeModulesInAnalysis():
            return(internalSymbols_declaredInModuleGlobalScope)

        module = self.__sourceFiles.getModule(moduleName)
        if module is None:
            printWarning('Module not found: ' + str(moduleName), 'ContainerDependenciesDetector')
            return(internalSymbols_declaredInModuleGlobalScope)

        subroutines_declaredInModuleGlobalScope  = [subroutineSimpleName for subroutineSimpleName in module.getSubroutines().keys()]
        derivedTypes_declaredInModuleGlobalScope = self.getDerivedTypes_declaredInModuleGlobalScope(moduleName)
        variables_declaredInModuleGlobalScope    = [variableName  for variableName, variable in module.getVariables().items() if not variable.isParameter()]
        parameters_declaredInModuleGlobalScope   = [parameterName for parameterName          in module.getParameters().keys()                              ]

        internalSymbols_declaredInModuleGlobalScope = subroutines_declaredInModuleGlobalScope  + \
                                                      derivedTypes_declaredInModuleGlobalScope + \
                                                      variables_declaredInModuleGlobalScope    + \
                                                      parameters_declaredInModuleGlobalScope
        
        return(internalSymbols_declaredInModuleGlobalScope)

    def getDependencies_inModuleGlobalScope(self, moduleName):
        dependencies_inModuleGlobalScope = defaultdict(UseStatementParts)

        if moduleName in self.__program.getExcludeModulesInAnalysis():
            return(dependencies_inModuleGlobalScope)

        module = self.__sourceFiles.getModule(moduleName)
        if module is None:
            printWarning('Module not found: ' + str(moduleName), 'ContainerDependenciesDetector')
            return(dependencies_inModuleGlobalScope)

        moduleGlobalScope_useStatements_with_ONLYEntities, moduleGlobalScope_useStatements_lackingEntities = module.getUseStatementsEntities()

        explicitDependencies = defaultdict(UseStatementParts)
        explicitDependencies.update(moduleGlobalScope_useStatements_with_ONLYEntities)
        
        implicitDependencies = self.findImplicitDependencies_createdBy_UseStatementsLackingEntities(moduleGlobalScope_useStatements_lackingEntities)

        dependencies_inModuleGlobalScope.update(explicitDependencies)
        dependencies_inModuleGlobalScope.update(implicitDependencies)
      
        return(dependencies_inModuleGlobalScope)

    def getDerivedTypes_declaredInModuleGlobalScope(self, moduleName):
        derivedTypes_declaredIn_moduleGlobalScope = []

        if moduleName in self.__program.getExcludeModulesInAnalysis():
            return(derivedTypes_declaredIn_moduleGlobalScope)

        module = self.__sourceFiles.getModule(moduleName) 
        if module is None:
            printWarning('Module not found: ' + str(moduleName), 'ContainerDependenciesDetector')
            return(derivedTypes_declaredIn_moduleGlobalScope)


        useRegEx = re.compile(r'^USE[\s\:]+(?P<modulename>[a-z0-9_]+)\s*(\,.*)?$', re.IGNORECASE);

        abstractTypes = self.__sourceFiles.getConfig().getAbstractTypesInAnalysis()
        for abstractType, subtype in abstractTypes.items():
            if isinstance(subtype, tuple) and len(subtype) == 2:
                        if self.__sourceFiles.existsModule(subtype[1]):
                            abstractTypes[abstractType] = subtype[0]
                        else:
                            abstractTypes[abstractType] = subtype[1]

        typeFinder = TypeFinder(abstractTypes)
        for i, statement, j in module.getStatementsBeforeContains():
            useRegExMatch = useRegEx.match(statement)
            if useRegExMatch is None:
                typeFinder.parseStatement(i, statement, j, module)

        collection        = typeFinder.getResult()
        typeDict, typeSet = collection.getResult()

        derivedTypes_declaredIn_moduleGlobalScope = [typE.getName().lower() for typE in list(typeSet)]
        return(derivedTypes_declaredIn_moduleGlobalScope)
