# coding=utf8
'''
@author: Mohammad Reza Heidari                                                                                                                                                                                                                
'''

import pickle
from callgraph        import CallGraph
from globals          import GlobalVariableTracker
from globaltypes      import RequiredGlobalDerivedTypesDetector
from globalparameters import GlobalParameterTracker
from source           import SubroutineFullName,                          \
                             FortranModulelNameSpace_PrintingNamePrefix,  \
                             ComponentImaginaryMasterEntrypointNameSpace, \
                             ComponentImaginaryMasterEntrypointFullName

class MinimalSubcomponentsExtraction():
      def __init__(self, sourceFiles, ndg, analysisConfig, graphBuilder, program):
          self.sourceFiles    = sourceFiles
          self.ndg            = ndg 
          self.analysisConfig = analysisConfig
          self.graphBuilder   = graphBuilder
          self.program        = program 
        
          dataFilePath                                    = self.analysisConfig.getDataPath()

          minimalNamespacesFilename                       = 'minimalns'
          excludedNamespacesFromMinimalNamespacesFilename = "exminimalns"
          calledSubroutinesFilename                       = 'calledsubs'
          minimalGlobalVariablesFilename                  = 'minimalvars'
          minimalGlobalParametersFilename                 = 'minimalpars'
          minimalGlobalTypesFilename                      = 'minimaltypes'
          minimalGlobalInterfacesFilename                 = 'minimalinterfaces'
          minimalGlobalAliasVarsFilename                  = 'minimalaliasvars'
                                                          
          carvedoutProgramFileExtension                   = '.cp'
          componentFileExtension                          = '.com'
          sharedCodesFileExtension                        = '.shd'
          
          self.carvedoutProgramMinimalNamespacesFilename                       = dataFilePath + minimalNamespacesFilename                       + carvedoutProgramFileExtension
          self.carvedoutProgramExcludedNamespacesFromMinimalNamespacesFilename = dataFilePath + excludedNamespacesFromMinimalNamespacesFilename + carvedoutProgramFileExtension 
          self.carvedoutProgramCalledSubroutinesFilename                       = dataFilePath + calledSubroutinesFilename                       + carvedoutProgramFileExtension
          self.carvedoutProgramMinimalVariablesFilename                        = dataFilePath + minimalGlobalVariablesFilename                  + carvedoutProgramFileExtension
          self.carvedoutProgramMinimalParametersFilename                       = dataFilePath + minimalGlobalParametersFilename                 + carvedoutProgramFileExtension 
          self.carvedoutProgramMinimalTypesFilename                            = dataFilePath + minimalGlobalTypesFilename                      + carvedoutProgramFileExtension 
          self.carvedoutProgramMinimalInterfacesFilename                       = dataFilePath + minimalGlobalInterfacesFilename                 + carvedoutProgramFileExtension    
          self.carvedoutProgramMinimalAliasVariablesFilename                   = dataFilePath + minimalGlobalAliasVarsFilename                  + carvedoutProgramFileExtension   

          self.componentMinimalNamespacesFilename                              = dataFilePath + minimalNamespacesFilename                       + componentFileExtension   
          self.componentExcludedNamespacesFromMinimalNamespacesFilename        = dataFilePath + excludedNamespacesFromMinimalNamespacesFilename + componentFileExtension        
          self.componentCalledSubroutinesFilename                              = dataFilePath + calledSubroutinesFilename                       + componentFileExtension   
          self.componentMinimalVariablesFilename                               = dataFilePath + minimalGlobalVariablesFilename                  + componentFileExtension   
          self.componentMinimalParametersFilename                              = dataFilePath + minimalGlobalParametersFilename                 + componentFileExtension   
          self.componentMinimalTypesFilename                                   = dataFilePath + minimalGlobalTypesFilename                      + componentFileExtension   
          self.componentMinimalInterfacesFilename                              = dataFilePath + minimalGlobalInterfacesFilename                 + componentFileExtension   
          self.componentMinimalAliasVariablesFilename                          = dataFilePath + minimalGlobalAliasVarsFilename                  + componentFileExtension   

          self.sharedCodesMinimalNamespacesFilename                            = dataFilePath + minimalNamespacesFilename                       + sharedCodesFileExtension
          self.sharedCodesExcludedNamespacesFromMinimalNamespacesFilename      = dataFilePath + excludedNamespacesFromMinimalNamespacesFilename + sharedCodesFileExtension
          self.sharedCodesCalledSubroutinesFilename                            = dataFilePath + calledSubroutinesFilename                       + sharedCodesFileExtension
          self.sharedCodesMinimalVariablesFilename                             = dataFilePath + minimalGlobalVariablesFilename                  + sharedCodesFileExtension
          self.sharedCodesMinimalParametersFilename                            = dataFilePath + minimalGlobalParametersFilename                 + sharedCodesFileExtension
          self.sharedCodesMinimalTypesFilename                                 = dataFilePath + minimalGlobalTypesFilename                      + sharedCodesFileExtension
          self.sharedCodesMinimalInterfacesFilename                            = dataFilePath + minimalGlobalInterfacesFilename                 + sharedCodesFileExtension
          self.sharedCodesMinimalAliasVariablesFilename                        = dataFilePath + minimalGlobalAliasVarsFilename                  + sharedCodesFileExtension

      def getResults(self):
          minimalSubcomponentExtraction = MinimalSubcomponentExtraction(self.sourceFiles, self.ndg, self.analysisConfig, self.graphBuilder)

          minimalSubcomponentExtraction.getResults (self.program.component       )
          minimalSubcomponentExtraction.getResults (self.program.carvedoutProgram)
          self.getSharedCodes()
          self.storeResults()

      def getSharedCodes(self):
          self.getSharedNamespaces()
          self.getSharedVariables ()

      def getSharedNamespaces(self):
          self.program.sharedCodes.namespaces.requred = set()
          for namespace in self.program.carvedoutProgram.namespaces.required:
              if namespace in self.program.component.namespaces.required:
                  self.program.sharedCodes.namespaces.required.append(namespace)

          self.program.sharedCodes.namespaces.excluded_from_required = set()
          for namespace in self.program.carvedoutProgram.namespaces.excluded_from_required:
              if namespace in self.program.component.namespaces.excluded_from_required:
                  self.program.sharedCodes.namespaces.excluded_from_required.add(namespace)

      def getSharedVariables(self):
          carvedoutProgramGlobalVariablesNames              = self.program.carvedoutProgram.globalVariablesFullNames
          componentGlobalVariablesNames                     = self.program.component.globalVariablesFullNames
          self.program.sharedCodes.globalVariablesFullNames = carvedoutProgramGlobalVariablesNames.intersection(componentGlobalVariablesNames)

      def loadResults(self):
          self.deserialize()

      def storeResults(self):
          self.serialize()

      def serialize(self):
          with open(self.carvedoutProgramMinimalNamespacesFilename,                      'wb') as fp:
               pickle.dump(self.program.carvedoutProgram.namespaces.required,               fp)

          with open(self.carvedoutProgramExcludedNamespacesFromMinimalNamespacesFilename,'wb') as fp:
               pickle.dump(self.program.carvedoutProgram.namespaces.excluded_from_required, fp)
 

          with open(self.carvedoutProgramMinimalVariablesFilename,                       'wb') as fp:
               pickle.dump(self.program.carvedoutProgram.globalVariablesFullNames,          fp)

          with open(self.carvedoutProgramMinimalParametersFilename,                      'wb') as fp:
               pickle.dump(self.program.carvedoutProgram.globalParametersFullNames,         fp)

          with open(self.carvedoutProgramMinimalTypesFilename,                           'wb') as fp:
               pickle.dump(self.program.carvedoutProgram.globalDerivedTypesFullNames,       fp)

          with open(self.carvedoutProgramMinimalInterfacesFilename,                      'wb') as fp:
               pickle.dump(self.program.carvedoutProgram.globalInterfacesFullNames,         fp)


          with open(self.componentMinimalNamespacesFilename,                             'wb') as fp:
               pickle.dump(self.program.component.namespaces.required,                      fp)

          with open(self.componentExcludedNamespacesFromMinimalNamespacesFilename,       'wb') as fp:
               pickle.dump(self.program.component.namespaces.excluded_from_required,        fp)

  
          with open(self.componentMinimalVariablesFilename,                              'wb') as fp:
               pickle.dump(self.program.component.globalVariablesFullNames,                 fp)

          with open(self.componentMinimalParametersFilename,                             'wb') as fp:
               pickle.dump(self.program.component.globalParametersFullNames,                fp)
  
          with open(self.componentMinimalTypesFilename,                                  'wb') as fp:
               pickle.dump(self.program.component.globalDerivedTypesFullNames,              fp)

          with open(self.componentMinimalInterfacesFilename,                             'wb') as fp:
               pickle.dump(self.program.component.globalInterfacesFullNames,                fp)


          with open(self.sharedCodesMinimalNamespacesFilename,                           'wb') as fp:
               pickle.dump(self.program.sharedCodes.namespaces.required,                    fp)
 
          with open(self.sharedCodesExcludedNamespacesFromMinimalNamespacesFilename,     'wb') as fp:
               pickle.dump(self.program.sharedCodes.namespaces.excluded_from_required,       fp)

          with open(self.sharedCodesMinimalVariablesFilename,                            'wb') as fp:
               pickle.dump(self.program.sharedCodes.globalVariablesFullNames,               fp)
 

      def deserialize(self):
          import pickle
 
          with open(self.carvedoutProgramMinimalNamespacesFilename,                      'rb') as fp:
               self.program.carvedoutProgram.namespaces.required               = pickle.load(fp)

          with open(self.carvedoutProgramExcludedNamespacesFromMinimalNamespacesFilename,'rb') as fp:
               self.program.carvedoutProgram.namespaces.excluded_from_required = pickle.load(fp)
        
          with open(self.carvedoutProgramCalledSubroutinesFilename,                      'rb') as fp:
               self.program.carvedoutProgram.calledSubroutinesFullNames        = pickle.load(fp)

          with open(self.carvedoutProgramMinimalVariablesFilename,                       'rb') as fp:
               self.program.carvedoutProgram.globalVariablesFullNames          = pickle.load(fp)

          with open(self.carvedoutProgramMinimalParametersFilename,                      'rb') as fp:
               self.program.carvedoutProgram.globalParametersFullNames         = pickle.load(fp)

          with open(self.carvedoutProgramMinimalTypesFilename,                           'rb') as fp:
               self.program.carvedoutProgram.globalDerivedTypesFullNames       = pickle.load(fp)

          with open(self.carvedoutProgramMinimalInterfacesFilename,                      'rb') as fp:
               self.program.carvedoutProgram.globalInterfacesFullNames         = pickle.load(fp)


          with open(self.componentMinimalNamespacesFilename,                             'rb') as fp:
               self.program.component.namespaces.required                      = pickle.load(fp)
 
          with open(self.componentExcludedNamespacesFromMinimalNamespacesFilename,       'rb') as fp:
               self.program.component.namespaces.excluded_from_required        = pickle.load(fp)

          with open(self.componentCalledSubroutinesFilename,                             'rb') as fp:
               self.program.component.calledSubroutinesFullNames               = pickle.load(fp)

          with open(self.componentMinimalVariablesFilename,                              'rb') as fp:
               self.program.component.globalVariablesFullNames                 = pickle.load(fp)

          with open(self.componentMinimalParametersFilename,                             'rb') as fp:
               self.program.component.globalParametersFullNames                = pickle.load(fp)

          with open(self.componentMinimalTypesFilename,                                  'rb') as fp:
               self.program.component.globalDerivedTypesFullNames              = pickle.load(fp)

          with open(self.componentMinimalInterfacesFilename,                             'rb') as fp:
               self.program.component.globalInterfacesFullNames                = pickle.load(fp)


          with open(self.sharedCodesMinimalNamespacesFilename,                           'rb') as fp:
               self.program.sharedCodes.namespaces.required                    = pickle.load(fp)
 
          with open(self.sharedCodesExcludedNamespacesFromMinimalNamespacesFilename,     'rb') as fp:
               self.program.sharedCodes.namespaces.excluded_from_required      = pickle.load(fp)

          with open(self.sharedCodesMinimalVariablesFilename,                            'rb') as fp:
               self.program.sharedCodes.globalVariablesFullNames               = pickle.load(fp)

      def printResults(self):
          self.loadResults()
          self.printNamespaces()
          self.printVariables ()

      def printNamespaces(self):
          print("\n\n\n*********************************** After Cleaning  ******************************************")
          print("***** Cleaned CarvedoutProgram Namespaces:")
          print(self.program.carvedoutProgram.namespaces.required, len(self.program.carvedoutProgram.namespaces.required))
          print("\n***** Cleaned Component  Namespaces:")
          print(self.program.component.namespaces.required       , len(self.program.component.namespaces.required       ))
          print("\n***** Shared Namespaces:")
          print(self.program.sharedCodes.namespaces.required     , len(self.program.sharedCodes.namespaces.required     ))
          
          print("\n")
          print("\n***** Excluded Namesapces from CarvedoutProgram:")
          print(self.program.carvedoutProgram.namespaces.excluded_from_required, len(self.program.carvedoutProgram.namespaces.excluded_from_required))
          print("\n***** Excluded Namespaces from Component:")
          print(self.program.component.namespaces.excluded_from_required,        len(self.program.component.namespaces.excluded_from_required       ))

          print("\n")
          print("***** CarvedoutProgram Minimal Namespaces: ", len(self.program.carvedoutProgram.namespaces.required))
          print("***** Component Minimal Namespaces       : ", len(self.program.component.namespaces.required       ))
          print("***** Shared Minimal Namespaces          : ", len(self.program.sharedCodes.namespaces.required     ))

      def printVariables(self):
          print("\n")
          print("***** CarvedoutProgram Global Variables : ", len(set(self.program.carvedoutProgram.globalVariablesFullNames)))
          print("***** Component Global Variables        : ", len(set(self.program.component.globalVariablesFullNames       )))
          print("***** Shared Global Variables           : ", len(set(self.program.sharedCodes.globalVariablesFullNames     )))

class MinimalSubcomponentExtraction():
      def __init__(self, sourceFiles, ndg, analysisConfig, graphBuilder):
          self.sourceFiles    = sourceFiles
          self.ndg            = ndg 
          self.analysisConfig = analysisConfig
          self.graphBuilder   = graphBuilder

          self.subcomponent     = None

      def resetResults(self):
          if self.subcomponent is None:
             return

          self.subcomponent.namespaces.required        = list()
          self.subcomponent.excluded_from_required     = set()
          self.subcomponent.globalVariables            = set()
          self.subcomponent.globalParameters           = set()
                                                        
          self.subcomponent.globalVariablesFullNames   = set()
          self.subcomponent.globalParametersFullNames  = set()
          self.subcomponent.globalDerivedTypesFullNames= set()
  
      def getResults(self, minimalSubcomponent):
          self.subcomponent = minimalSubcomponent
          self.resetResults() 
          self.getParts()   

      def getParts(self):
          self.getGlobalVariables   ()
          self.getGlobalDerivedTypes()
          self.getGlobalParameters  ()
          self.getNamespaces        ()
  
      def getCalledSubroutines(self):
         return

         if self.subcomponent is None:
            return

         self.subcomponent.subroutines                = set()
         self.subcomponent.calledSubroutinesFullNames = set()

         self.subcomponent.callGraph = CallGraph(self.ndg)
         callGraph                   = self.subcomponent.callGraph
         isCarveoutProgram           = self.subcomponent.isCarveoutProgram

         if isCarveoutProgram:
             subcomponentEntrypointFullName = self.subcomponent.entrypointsFullNames
             self.subcomponent.callGraph    = self.graphBuilder.buildCallGraph(subcomponentEntrypointFullName, callGraph, isCarveoutProgram, clear = self.analysisConfig.getClearCache())
         else:
             componentImaginaryMasterEntrypointFullName = SubroutineFullName(ComponentImaginaryMasterEntrypointFullName)
             self.subcomponent.callGraph.addSubroutine(componentImaginaryMasterEntrypointFullName)

             for subcomponentEntrypointFullName in self.subcomponent.entrypointsFullNames:
                 self.subcomponent.callGraph.addCall(componentImaginaryMasterEntrypointFullName, subcomponentEntrypointFullName, -1, 0);
                 self.subcomponent.callGraph = self.graphBuilder.buildCallGraph(subcomponentEntrypointFullName, callGraph, isCarveoutProgram, clear = self.analysisConfig.getClearCache())
                 self.ndg.addEdge(ComponentImaginaryMasterEntrypointNameSpace, subcomponentEntrypointFullName.getNameSpace())

         self.subcomponent.subroutines                = set(self.subcomponent.callGraph.getAllSubroutineNames())
         self.subcomponent.calledSubroutinesFullNames = set(["__"+subroutine.getModuleName().upper()+"_MOD_"+subroutine.getSimpleName().upper() for subroutine in self.subcomponent.subroutines])
     
      def getGlobalInterfaces(self):
          return
          if self.subcomponent is None:
              return

          self.subcomponent.globalInterfacesFullNames = set()

          for interfaceName, interface in self.subcomponent.interfaces.items():
              interfaceFullName = "__" + interface.getModuleName().upper() + "_MOD_" + interfaceName.upper()
              for procedureName in interface.getProcedures():
                  interfaceProcedureFullName = "__" + interface.getModuleName().upper() + "_MOD_" + procedureName.upper()
                  if interfaceProcedureFullName in self.subcomponent.calledSubroutinesFullNames:
                      self.subcomponent.globalInterfacesFullNames.add(interfaceFullName)
                      break 

      def getGlobalVariables(self):
          if self.subcomponent is None:
              return
          self.subcomponent.globalVariables                              = set()
          self.subcomponent.globalVariablesFullNames                     = set()

          globalVariableTracker = GlobalVariableTracker(self.sourceFiles,                                            \
                                                        self.ndg,                                                    \
                                                        self.analysisConfig,                                         \
                                                        self.subcomponent.callGraph.getGlobalNamespaceSubprograms(), \
                                                        self.subcomponent.isCarveoutProgram,                         \
                                                        self.analysisConfig.getClearCache(),                         \
                                                        callGraphBuilder = self.graphBuilder)
          self.subcomponent.globalVariables = globalVariableTracker.getGlobalVariables(self.subcomponent.callGraph)
          for var in self.subcomponent.globalVariables:                 
              self.subcomponent.globalVariablesFullNames.add(var.getOriginalFullName())

          self.subcomponent.namespaces.excluded_from_required.update((globalVariableTracker.getVisitedExcludedModules()))
 
      def getGlobalParameters(self):
          if self.subcomponent is None:
              return

          self.subcomponent.globalParameters          = set()
          self.subcomponent.globalParametersFullNames = set()

          globalParameterTracker = GlobalParameterTracker(self.sourceFiles, 
                                                          self.ndg, 
                                                          self.analysisConfig, 
                                                          self.analysisConfig.getClearCache(), 
                                                          callGraphBuilder = self.graphBuilder)
      
          self.subcomponent.globalParameters = globalParameterTracker.getGlobalParameters(self.subcomponent.callGraph, 
                                                                                          self.subcomponent.callGraph.getGlobalNamespaceSubprograms(), 
                                                                                          self.subcomponent.isCarveoutProgram)
          
          for var in self.subcomponent.globalVariables:
               self.subcomponent.globalParameters.extend(var.getUsedParameters())
 
          for par in self.subcomponent.globalParameters:
               self.subcomponent.globalParameters.extend(par.getUsedParameters())

          for par in self.subcomponent.globalParameters:
              self.subcomponent.globalParametersFullNames.add(par.getOriginalFullName())

          self.subcomponent.namespaces.excluded_from_required.update(globalParameterTracker.getVisitedExcludedModules())
      
      def getGlobalDerivedTypes(self):
          if self.subcomponent is None:
              return

          self.subcomponent.globalDerivedTypesFullNames = set()

          globalDerivedTypesDetector = RequiredGlobalDerivedTypesDetector(self.analysisConfig, self.subcomponent, self.sourceFiles)
          self.subcomponent.globalDerivedTypesFullNames = globalDerivedTypesDetector.getResults()
          self.subcomponent.namespaces.excluded_from_required.update(globalDerivedTypesDetector.getVisitedExcludedModules())

      def getNamespaces(self):
          if self.subcomponent is None:
              return

          self.subcomponent.namespaces.required = list()
          requiredNamespaces = set()

          for subroutineFullName in self.subcomponent.subroutines:
              namespaceName         = subroutineFullName.getNameSpace()
              namespacePrintingName = subroutineFullName.getNameSpacePrintingName()
              if namespaceName in self.analysisConfig.getExcludeModulesInAnalysis() or subroutineFullName in self.analysisConfig.getExcludedSubroutinesInAnalysis():
                  self.subcomponent.namespaces.excluded_from_required.add(namespacePrintingName)
                  continue
              requiredNamespaces.add(namespacePrintingName)
    
          for var in self.subcomponent.globalVariables:
              namespaceName         = var.getModuleName().lower()
              namespacePrintingName = FortranModulelNameSpace_PrintingNamePrefix+var.getModuleName().lower()
              if namespaceName in self.analysisConfig.getExcludeModulesInAnalysis():
                  self.subcomponent.namespaces.excluded_from_required.add(namespacePrintingName)
                  continue
              requiredNamespaces.add(namespacePrintingName)
    
          for typeName in self.subcomponent.globalDerivedTypesFullNames:
              namespaceName         = typeName[2:typeName.find('_MOD_')].lower()
              namespacePrintingName = FortranModulelNameSpace_PrintingNamePrefix+namespaceName
              if namespaceName in self.analysisConfig.getExcludeModulesInAnalysis():
                  self.subcomponent.namespaces.excluded_from_required.add(namespacePrintingName)
                  continue
              requiredNamespaces.add(namespacePrintingName)
    
          for par in self.subcomponent.globalParameters:
              namespaceName         = par.getModuleName().lower()
              namespacePrintingName = FortranModulelNameSpace_PrintingNamePrefix+par.getModuleName().lower()
              if namespaceName in self.analysisConfig.getExcludeModulesInAnalysis():
                  self.subcomponent.namespaces.excluded_from_required.add(namespacePrintingName)
                  continue
              requiredNamespaces.add(namespacePrintingName)

          supersetNamespacePrintingNames = list()
          for namespaceName in self.subcomponent.namespaces.superSet:
              supersetNamespacePrintingNames.append(self.sourceFiles.getNamespacePrintingName_from_namespaceName(namespaceName))

          for namespacePrintingName in supersetNamespacePrintingNames:
              if namespacePrintingName in requiredNamespaces:
                  self.subcomponent.namespaces.required.append(namespacePrintingName)
                  requiredNamespaces.remove                   (namespacePrintingName)

          for namespacePrintingName in self.subcomponent.namespaces.excluded_from_superSet:
              self.subcomponent.namespaces.excluded_from_required.add(namespacePrintingName)
