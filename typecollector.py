# coding=utf8

import re
from assertions import assertType, assertTypeAll
from source import SubroutineFullName, SourceFiles, FortranProgramMainUnit, FortranProgramGlobalNameSpace, FortranModuleNameSpaceType
from interfaces import InterfaceFinder
from typefinder import TypeFinder
from supertypes import UseTraversalPassenger
from printout import printWarning

class UseTraversal(object):
    
    __moduleWarnings = set()

    def __init__(self, sourceFiles, ndg, programSettings, excludeModules = [], abstractTypes = {}, globalNamespaceSubprograms = {} , carvedoutProgramExtractionEnabled = False):
        assertType(sourceFiles, 'sourceFiles', SourceFiles)
        assertTypeAll(excludeModules, 'excludeModules', str)
        assertType(abstractTypes, 'abstractTypes', dict)

        self.__sourceFiles = sourceFiles;
        self.__excludeModules = [m.lower() for m in excludeModules]
        self.__additionalModules = set()
        for abstractType, subtype in abstractTypes.items():
            if isinstance(subtype, tuple) and len(subtype) == 2:
                if self.__sourceFiles.existsModule(subtype[1]):
                    self.__additionalModules.add(subtype[1])
                    abstractTypes[abstractType] = subtype[0]
                else:
                    self.__additionalModules.add(subtype[0])
                    abstractTypes[abstractType] = subtype[1]
        self.__visitedModules = set()
        self.__interfaceFinder = InterfaceFinder()
        self.__typeFinder = TypeFinder(abstractTypes)
        self.__passengers = [self.__interfaceFinder, self.__typeFinder]
        self.__globalNamespaceSubprograms = globalNamespaceSubprograms
        self.__carvedoutProgramExtractionEnabled = carvedoutProgramExtractionEnabled
        self.__ndg = ndg
        self.__programSettings = programSettings

    def addPassenger(self, passenger):
        assertType(passenger, 'passenger', UseTraversalPassenger)
        
        self.__passengers.append(passenger)

    def parseModules(self, rootSubroutine):
        assertType(rootSubroutine, 'rootSubroutine', SubroutineFullName)
        self.__reset()
        self.__parseModulesRecursive(rootSubroutine.getModuleName(), rootSubroutine.getSimpleName(), rootSubroutine.getNameSpaceType(), False, None)
        for moduleName in self.__additionalModules:
            if moduleName not in self.__visitedModules:
                self.__parseModulesRecursive(moduleName, None, FortranModuleNameSpaceType, False, None)

        for globalNamespaceSubprogram in self.__globalNamespaceSubprograms:
            moduleName           = globalNamespaceSubprogram.getModuleName()
            subroutineSimpleName = globalNamespaceSubprogram.getSimpleName()
            nameSpaceType        = globalNamespaceSubprogram.getNameSpaceType()
            if moduleName not in self.__visitedModules:
                if subroutineSimpleName not in self.__excludedSubroutinesInAnalysis:
                   if isinstance(globalNamespaceSubprogram, InnerSubroutineName):
                       isInnerSubroutine        = True
                       hostSubroutineSimpleName = globalNamespaceSubprogram.getHostName().getSimpleName() 
                   else:
                       isInnerSubroutine        = False
                       hostSubroutineSimpleName = None
                   self.__parseModulesRecursive(moduleName, subroutineSimpleName, nameSpaceType, isInnerSubroutine, hostSubroutineSimpleName)

    def __parseModulesRecursive(self, moduleName, subroutineName, nameSpaceType, isInnerSubroutine, hostSubroutineSimpleName, parent = ''):
        useRegEx     = re.compile(r'^USE[\s\:]+(?P<modulename>[a-z0-9_]+)\s*(\,.*)?$', re.IGNORECASE);

        useOnlyRegRE = re.compile(r"^USE\s+[a-z0-9_]+\s*\,\s*ONLY\s*\:\s*(?P<statements1>[\w]+\s*)(?P<statements2>(\s*\,\s*[\w]+\s*){0,})", re.IGNORECASE)

        usedModules = set()

        if moduleName in self.__visitedModules:
            return

        self.__visitedModules.add(moduleName)

        module = self.__sourceFiles.findModule(moduleName,subroutineName,nameSpaceType, isInnerSubroutine, hostSubroutineSimpleName)
        if module is not None:
            for i, statement, j in module.getStatements():
                useRegExMatch = useRegEx.match(statement)
                if useRegExMatch is not None:
                    usedModuleName = useRegExMatch.group('modulename')
                    if usedModuleName.lower() not in self.__excludeModules:
                        modulePermitted = True
                        if self.__carvedoutProgramExtractionEnabled and usedModuleName.lower() in self.__programSettings.getComponentEntrypointsModules():
                            modulePermitted = False
                            usedModuleRegRE=useOnlyRegRE.match(statement)
                            if usedModuleRegRE is None:
                                raise ValueError('Wrong used module of the component entry point! Correct usage: USE modulename, ONLY : subroutine\'s name')
                            s1=usedModuleRegRE.group("statements1")
                            s2=usedModuleRegRE.group("statements2")
                            s3=s1+s2
                            moduleEntities = re.findall(r'[^,\s]+', s3)
                            for moduleEntity in moduleEntities:
                                if moduleEntity not in self.__programSettings.getComponententEntrypoints()[usedModuleName]:
                                    modulePermitted = True
                                    break

                        if modulePermitted:
                            usedModules.add(usedModuleName)
                            if nameSpaceType==FortranProgramMainUnit or nameSpaceType==FortranProgramGlobalNameSpace:
                                self.__ndg.addEdge(nameSpaceType+subroutineName,usedModuleName)
                            else:
                                self.__ndg.addEdge(moduleName,usedModuleName)
                                                                                   
                else:
                    self.__parseStatement(i, statement, j, module)
        elif moduleName not in UseTraversal.__moduleWarnings:
            UseTraversal.__moduleWarnings.add(moduleName)
            warning = 'Source file not found for module: ' + moduleName
            if parent:
                warning += ' (in: ' + parent + ')'
            printWarning(warning, 'UseTraversal')

        for usedModule in usedModules:
            self.__parseModulesRecursive(usedModule, None, FortranModuleNameSpaceType, False, None)

    def __parseStatements(self, moduleName, subroutineName, nameSpaceType, parent = ''):
        useRegEx     = re.compile(r'^USE[\s\:]+(?P<modulename>[a-z0-9_]+)\s*(\,.*)?$', re.IGNORECASE);

        useOnlyRegRE = re.compile(r"^USE\s+[a-z0-9_]+\s*\,\s*ONLY\s*\:\s*(?P<statements1>[\w]+\s*)(?P<statements2>(\s*\,\s*[\w]+\s*){0,})", re.IGNORECASE)

        for i, statement, j in statements:
            useRegExMatch = useRegEx.match(statement)
            if useRegExMatch is None:
                self.__parseStatement(i, statement, j, None)
    
    def __parseStatement(self, i, statement, j, moduleName):
        for passenger in self.__passengers:
            passenger.parseStatement(i, statement, j, moduleName)
            
    def getInterfaces(self):
        return self.__interfaceFinder.getResult()
            
    def getTypes(self):
        return self.__typeFinder.getResult()
        
    def __reset(self):
        self.__visitedModules = set()
        for passenger in self.__passengers:
            passenger.reset()
        
