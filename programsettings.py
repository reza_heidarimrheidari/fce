# coding=utf8

from __future__ import print_function
import os
from collections import defaultdict
from printout    import printError, printErrorAndExit
from source      import SubroutineFullName,  FortranProgramMainUnit

CFG_SOURCE_DIRS                                         = 'SOURCE_DIRS'
CFG_SOURCE_DIRS_LEGACY                                  = 'SOURCE_DIR'
CFG_DATA_DIR                                            = 'DATA_DIR'
CFG_ASSEMBLER_DIRS                                      = 'ASSEMBLER_DIRS'
CFG_ASSEMBLER_DIRS_LEGACY                               = 'ASSEMBLER_DIR'
CFG_SPECIAL_MODULE_FILES                                = 'SPECIAL_MODULE_FILES'
CFG_SOURCE_FILES_PREPROCESSED                           = 'SOURCE_FILES_PREPROCESSED'
CFG_CACHE_DIR                                           = 'CACHE_DIR'
CFG_EXCLUDE_MODULES                                     = 'EXCLUDE_MODULES'
CFG_IGNORE_GLOBALS_FROM_MODULES                         = 'IGNORE_GLOBALS_FROM_MODULES'
CFG_IGNORE_DERIVED_TYPES                                = 'IGNORE_DERIVED_TYPES'
CFG_ABSTRACT_TYPES                                      = 'ABSTRACT_TYPE_IMPLEMENTATIONS'
CFG_ALWAYS_FULL_TYPES                                   = 'ALWAYS_FULL_TYPES'
CFG_EXCLUDE_SUBROUTINES                                 = 'EXCLUDE_SUBROUTINES'
CFG_GLOBALSUBROUTINES_FILES                             = 'GLOBALSUBROUTINES_FILES'
CFG_PROGRAMMAINUNIT_NAME                                = 'PROGRAM_MAINUNIT_NAME'
CFG_COMPONENT_ENTRYPOINTS                               = 'COMPONENT_ENTRYPOINTS'
CFG_TARGETMODULE_FOR_REDUNDANCY_AND_DEPENDENCY_ANALYSIS = 'CFG_TARGETMODULE_FOR_REDUNDANCY_AND_DEPENDENCY_ANALYSIS'

class ProgramSettings(object):

      def  __init__(self):
           self.__config = self.loadFromFile()

           self.__sourceDirs                                       = self.__config[CFG_SOURCE_DIRS]
           self.__dataPath                                         = self.__config[CFG_DATA_DIR]
           self.__assemblerCodePath                                = self.__config[CFG_ASSEMBLER_DIRS]
           self.__specialFortranModulesFiles                       = self.__config[CFG_SPECIAL_MODULE_FILES]
           self.__globalFortranSubroutinesFiles                    = self.__config[CFG_GLOBALSUBROUTINES_FILES]
           self.__fortranSourceCodePath                            = self.__config[CFG_SOURCE_DIRS]
           self.__fortranSourceCodePreprocessors                   = self.__config[CFG_SOURCE_FILES_PREPROCESSED]
           self.__excludeModules                                   = self.__config[CFG_EXCLUDE_MODULES]
           self.__ignoreGlobalsFromModules                         = self.__config[CFG_IGNORE_GLOBALS_FROM_MODULES]
           self.__ignoreDerivedTypes                               = self.__config[CFG_IGNORE_DERIVED_TYPES]
           self.__alwaysFullTypes                                  = self.__config[CFG_ALWAYS_FULL_TYPES]
           self.__abstractTypes                                    = self.__config[CFG_ABSTRACT_TYPES]
           self.__targetModule_for_redundancyAndDependencyAnalysis = self.__config[CFG_TARGETMODULE_FOR_REDUNDANCY_AND_DEPENDENCY_ANALYSIS]
           self.__clearCache                                       = True

           self.__excludedSubroutinesFullnames = []
           for excludedGloblaSubroutineSimpleName in self.__config[CFG_EXCLUDE_SUBROUTINES]:
               excludedGloblaSubroutineFullName = SubroutineFullName.fromGlobalSuroutineName(excludedGloblaSubroutineSimpleName)
               self.__excludedSubroutinesFullnames.append(excludedGloblaSubroutineFullName._name)

           if SubroutineFullName.validParts(FortranProgramMainUnit, self.__config[CFG_PROGRAMMAINUNIT_NAME]):
               self.__programMainUnit_Fullname = SubroutineFullName.fromParts(FortranProgramMainUnit, self.__config[CFG_PROGRAMMAINUNIT_NAME])
           else:
               printErrorAndExit(4, 'Invalid Program Main Unit')

           self.__componententEntrypoints = defaultdict(set)         
           for module,subroutines in self.__config[CFG_COMPONENT_ENTRYPOINTS]:
               for subroutine in subroutines:
                   if SubroutineFullName.validParts(module,subroutine):
                       self.__componententEntrypoints[module.lower()].add(subroutine.lower())
                   else:   
                       printErrorAndExit(4, 'Invalid component entry point name')      

           self.__componentEntrypointsFullNames = [SubroutineFullName.fromParts(moduleName,subroutineName) for moduleName in self.__componententEntrypoints for subroutineName in self.__componententEntrypoints[moduleName] if subroutineName]
           self.__componentEntrypointsModules   = [moduleName.lower() for moduleName in self.__componententEntrypoints]

      def loadFromFile(configFile = "", incomplete = False, baseConfig = {}):
          configFile = 'fceconfig.py'
 
          originalConfigFile = configFile
          if not os.path.isfile(configFile):
              configFile = os.path.join(os.path.dirname(os.path.realpath(__file__)), originalConfigFile)
          if not os.path.isfile(configFile):
              configFile = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'config', originalConfigFile)
          if not os.path.isfile(configFile):
              printError('Config file not found: ' + originalConfigFile, location='FortranCallGraph')
              return None
      
          config = baseConfig
      
          with open(configFile) as f:
              code = compile(f.read(), configFile, 'exec')
              globalNamespace = globals()
              exec(code, globalNamespace, config)
      
          configError = False
          if CFG_SOURCE_DIRS not in config and CFG_SOURCE_DIRS_LEGACY in config:
              config[CFG_SOURCE_DIRS] = config[CFG_SOURCE_DIRS_LEGACY]
          if CFG_SOURCE_DIRS not in config or not config[CFG_SOURCE_DIRS]:
              if not incomplete:
                  printError('Missing config variable: ' + CFG_SOURCE_DIRS, location='FortranCallGraph')
                  configError = True
          elif isinstance(config[CFG_SOURCE_DIRS], str):
              config[CFG_SOURCE_DIRS] = [config[CFG_SOURCE_DIRS]]
      
          if CFG_ASSEMBLER_DIRS not in config and CFG_ASSEMBLER_DIRS_LEGACY in config:
              config[CFG_ASSEMBLER_DIRS] = config[CFG_ASSEMBLER_DIRS_LEGACY]
          if CFG_ASSEMBLER_DIRS not in config or not config[CFG_ASSEMBLER_DIRS]:
              if not incomplete:
                  printError('Missing config variable: ' + CFG_ASSEMBLER_DIRS, location='FortranCallGraph')
                  configError = True
          elif isinstance(config[CFG_ASSEMBLER_DIRS], str):
              config[CFG_ASSEMBLER_DIRS] = [config[CFG_ASSEMBLER_DIRS]]
      
          if CFG_SPECIAL_MODULE_FILES not in config or not config[CFG_SPECIAL_MODULE_FILES]:
              config[CFG_SPECIAL_MODULE_FILES] = {}
      
          if CFG_SOURCE_FILES_PREPROCESSED not in config or not config[CFG_SOURCE_FILES_PREPROCESSED]:
              config[CFG_SOURCE_FILES_PREPROCESSED] = False
      
          if CFG_CACHE_DIR not in config or not config[CFG_CACHE_DIR]:
              config[CFG_CACHE_DIR] = None
      
          if CFG_EXCLUDE_MODULES not in config or not config[CFG_EXCLUDE_MODULES]:
              config[CFG_EXCLUDE_MODULES] = []
      
          if CFG_IGNORE_GLOBALS_FROM_MODULES not in config or not config[CFG_IGNORE_GLOBALS_FROM_MODULES]:
              config[CFG_IGNORE_GLOBALS_FROM_MODULES] = []
      
          if CFG_IGNORE_DERIVED_TYPES not in config or not config[CFG_IGNORE_DERIVED_TYPES]:
              config[CFG_IGNORE_DERIVED_TYPES] = []
      
          if CFG_ALWAYS_FULL_TYPES not in config or not config[CFG_ALWAYS_FULL_TYPES]:
              config[CFG_ALWAYS_FULL_TYPES] = []
      
          if CFG_ABSTRACT_TYPES not in config or not config[CFG_ABSTRACT_TYPES]:
              config[CFG_ABSTRACT_TYPES] = {}
      
          if CFG_GLOBALSUBROUTINES_FILES not in config or not config[CFG_GLOBALSUBROUTINES_FILES]:
              config[CFG_GLOBALSUBROUTINES_FILES] = {}

          if CFG_PROGRAMMAINUNIT_NAME not in config or not config[CFG_PROGRAMMAINUNIT_NAME]:
              config[CFG_PROGRAMMAINUNIT_NAME] = ""

          if CFG_COMPONENT_ENTRYPOINTS not in config or not isinstance(config[CFG_COMPONENT_ENTRYPOINTS],list):
              config[CFG_COMPONENT_ENTRYPOINTS] = [("",{""})]

          if CFG_EXCLUDE_SUBROUTINES not in config or not isinstance(config[CFG_EXCLUDE_SUBROUTINES],list):
              config[CFG_EXCLUDE_SUBROUTINES] = []

          if CFG_TARGETMODULE_FOR_REDUNDANCY_AND_DEPENDENCY_ANALYSIS not in config or not isinstance(config[CFG_TARGETMODULE_FOR_REDUNDANCY_AND_DEPENDENCY_ANALYSIS], list):
              config[CFG_TARGETMODULE_FOR_REDUNDANCY_AND_DEPENDENCY_ANALYSIS] = []

          if configError:
              return None
      
          return config

      def  getClearCache(self):
           return(self.__clearCache)

      def  getConfig(self):
           return(self.__config)

      def  getSourceDirs(self):
           return(self.__sourceDirs)

      def  getDataPath(self):
           return(self.__dataPath)

      def  getAssemblerCodePath(self):
           return(self.__assemblerCodePath)

      def  getSpecialFortranModulesFiles(self):
           return(self.__specialFortranModulesFiles)

      def  getGlobalFortranSubroutinesFiles(self):
           return(self.__globalFortranSubroutinesFiles)

      def  getFortranSourceCodePath(self):
           return(self.__fortranSourceCodePath)

      def  getFortranSourceCodePreprocessors(self):
           return(self.__fortranSourceCodePreprocessors)

      def  getExcludeModulesInAnalysis(self):
           return(self.__excludeModules)

      def  getIgnoreGlobalsFromModulessInAnalysis(self):
           return(self.__ignoreGlobalsFromModules)

      def  getIgnoreDerivedTypesInAnalysis(self):
           return(self.__ignoreDerivedTypes)

      def  getAlwaysFullTypesInAnalysis(self):
           return(self.__alwaysFullTypes)

      def  getAbstractTypesInAnalysis(self):
           return(self.__abstractTypes)

      def  getMainProgramUnitFullname(self):
           return(self.__programMainUnit_Fullname)

      def  getComponententEntrypoints(self):
           return(self.__componententEntrypoints)

      def  getComponentEntrypointsFullNames(self):
           return(self.__componentEntrypointsFullNames)

      def  getComponentEntrypointsFullNamesString(self):
           entryPointFullNameStringList = [entryPointFullName._name for entryPointFullName in self.getComponentEntrypointsFullNames()]

           return(entryPointFullNameStringList)

      def  getComponentEntrypointsModules(self):
           return(self.__componentEntrypointsModules)

      def  getExcludedSubroutinesInAnalysis(self):
           return(self.__excludedSubroutinesFullnames)

      def  getTargetModule_for_redundancyAndDependencyAnalysis(self):
           return(self.__targetModule_for_redundancyAndDependencyAnalysis)
