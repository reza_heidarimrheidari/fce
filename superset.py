# coding=utf8
'''                                                                                                                                                                                                                                           
@author: Mohammad Reza Heidari                                                                                                                                                                                                                
'''

from callgraph  import CallGraph
from useprinter import UsedModuleNamePrinter
from source     import SubroutineFullName,                          \
                       FortranProgramGlobalNameSpace,               \
                       ComponentImaginaryMasterEntrypointNameSpace, \
                       ComponentImaginaryMasterEntrypointFullName

class SupersetSubcomponentsExtraction():
      def __init__(self, sourceFiles, ndg, analysisConfig, graphBuilder, program):
          self.sourceFiles    = sourceFiles
          self.ndg            = ndg
          self.analysisConfig = analysisConfig
          self.graphBuilder   = graphBuilder
          self.program        = program 

          dataFilePath                                     = self.analysisConfig.getDataPath()

          supersetNamespacesFilename                       = 'supersetns'
          supersetVariablesFilename                        = 'supersetvars'
          supersetDerivedTypesFilename                     = 'supersettypes'
          calledSubroutinesFilename                        = 'calledsubs' 
          excludedNamespacesFromSupersetNamespacesFilename = "exsupersetns"

          carvedoutProgramFileExtension                    = '.cp'
          componentFileExtension                           = '.com'
          sharedCodesFileExtension                         = '.shd'
          
          self.carvedoutProgramSupersetNamespacesFilename                       = dataFilePath + supersetNamespacesFilename                       + carvedoutProgramFileExtension 
          self.componentSupersetNamespacesFilename                              = dataFilePath + supersetNamespacesFilename                       + componentFileExtension        
          self.sharedCodesSupersetNamespacesFilename                            = dataFilePath + supersetNamespacesFilename                       + sharedCodesFileExtension

          self.carvedoutProgramExcludedNamespacesFromSupersetNamespacesFilename = dataFilePath + excludedNamespacesFromSupersetNamespacesFilename + carvedoutProgramFileExtension 
          self.componentExcludedNamespacesFromSupersetNamespacesFilename        = dataFilePath + excludedNamespacesFromSupersetNamespacesFilename + componentFileExtension        
          self.sharedCodesExcludedNamespacesFromSupersetNamespacesFilename      = dataFilePath + excludedNamespacesFromSupersetNamespacesFilename + sharedCodesFileExtension
                                                                                                           
          self.carvedoutProgramCalledSubroutinesFilename                       = dataFilePath + calledSubroutinesFilename                         + carvedoutProgramFileExtension 
          self.componentCalledSubroutinesFilename                              = dataFilePath + calledSubroutinesFilename                         + componentFileExtension
          self.sharedCodesCalledSubroutinesFilename                            = dataFilePath + calledSubroutinesFilename                         + sharedCodesFileExtension
                                                                                                                                                  
          self.carvedoutProgramSupersetVariablesFilename                       = dataFilePath + supersetVariablesFilename                         + carvedoutProgramFileExtension 
          self.componentSupersetVariablesFilename                              = dataFilePath + supersetVariablesFilename                         + componentFileExtension
          self.sharedCodesSupersetVariablesFilename                            = dataFilePath + supersetVariablesFilename                         + sharedCodesFileExtension
                                                                                                                                                  
          self.carvedoutProgramSupersetDerivedTypesFilename                    = dataFilePath + supersetDerivedTypesFilename                      + carvedoutProgramFileExtension 
          self.componentSupersetDerivedTypesFilename                           = dataFilePath + supersetDerivedTypesFilename                      + componentFileExtension
          self.sharedCodesSupersetDerivedTypesFilename                         = dataFilePath + supersetDerivedTypesFilename                      + sharedCodesFileExtension

      def getResults(self):
          supersetSubcomponentExtraction= SupersetSubcomponentExtraction(self.sourceFiles, self.ndg, self.analysisConfig, self.graphBuilder)
          supersetSubcomponentExtraction.getResults (self.program.component       )
          supersetSubcomponentExtraction.getResults (self.program.carvedoutProgram)
          self.getSharedCodes()
          self.storeResults()

      def getSharedCodes(self):
          self.getSharedNamespaces()
          self.getSharedVariables ()

      def getSharedNamespaces(self):
          self.program.sharedCodes.namespaces.superSet = list()
          for namespace in self.program.carvedoutProgram.namespaces.superSet:
              if namespace in self.program.component.namespaces.superSet:
                  self.program.sharedCodes.namespaces.superSet.append(namespace)

          self.program.sharedCodes.namespaces.excluded_from_superSet = set()
          for namespace in self.program.carvedoutProgram.namespaces.excluded_from_superSet:
              if namespace in self.program.component.namespaces.excluded_from_superSet:
                  self.program.sharedCodes.namespaces.excluded_from_superSet.add(namespace)

      def getSharedVariables(self):
          carvedoutProgramGlobalVariablesNames                      = self.program.carvedoutProgram.supersetGlobalVariablesFullNames
          componentGlobalVariablesNames                             = self.program.component.supersetGlobalVariablesFullNames
          self.program.sharedCodes.supersetGlobalVariablesFullNames = carvedoutProgramGlobalVariablesNames.intersection(componentGlobalVariablesNames)


      def loadResults(self):
          self.deserialize()

      def storeResults(self):
          self.serialize()

      def serialize(self):
          import pickle

          with open(self.carvedoutProgramSupersetNamespacesFilename,                       'wb') as fp:
               pickle.dump(self.program.carvedoutProgram.namespaces.superSet,              fp)
                                                                                           
          with open(self.componentSupersetNamespacesFilename,                              'wb') as fp:              
               pickle.dump(self.program.component.namespaces.superSet,                     fp)
                                                                                           
          with open(self.sharedCodesSupersetNamespacesFilename,                            'wb') as fp:              
               pickle.dump(self.program.sharedCodes.namespaces.superSet,                   fp)


          with open(self.carvedoutProgramExcludedNamespacesFromSupersetNamespacesFilename, 'wb') as fp:              
               pickle.dump(self.program.carvedoutProgram.namespaces.excluded_from_superSet, fp)          
          
          with open(self.componentExcludedNamespacesFromSupersetNamespacesFilename,        'wb') as fp:  
               pickle.dump(self.program.component.namespaces.excluded_from_superSet,        fp)
            
          with open(self.sharedCodesExcludedNamespacesFromSupersetNamespacesFilename,      'wb') as fp:              
               pickle.dump(self.program.sharedCodes.namespaces.excluded_from_superSet,      fp)
                 

          with open(self.carvedoutProgramCalledSubroutinesFilename,                        'wb') as fp:
               pickle.dump(self.program.carvedoutProgram.calledSubroutinesFullNames,       fp)
                                                                                           
          with open(self.componentCalledSubroutinesFilename,                               'wb') as fp:          
               pickle.dump(self.program.component.calledSubroutinesFullNames,              fp)
                                                                                           

          with open(self.carvedoutProgramSupersetVariablesFilename,                        'wb') as fp:
               pickle.dump(self.program.carvedoutProgram.supersetGlobalVariablesFullNames, fp)

          with open(self.componentSupersetVariablesFilename,                               'wb') as fp:
               pickle.dump(self.program.component.supersetGlobalVariablesFullNames,        fp)

          with open(self.sharedCodesSupersetVariablesFilename,                             'wb') as fp:
               pickle.dump(self.program.sharedCodes.supersetGlobalVariablesFullNames,      fp)


          with open(self.carvedoutProgramSupersetDerivedTypesFilename,                     'wb') as fp:
               pickle.dump(self.program.carvedoutProgram.supersetGlobalDerivedTypesFullNames, fp)

          with open(self.componentSupersetDerivedTypesFilename,                            'wb') as fp:
               pickle.dump(self.program.component.supersetGlobalDerivedTypesFullNames,        fp)


      def deserialize(self):
          import pickle

          with open(self.carvedoutProgramSupersetNamespacesFilename,                       'rb') as fp:
               self.program.carvedoutProgram.namespaces.superSet                = pickle.load(fp)

          with open(self.componentSupersetNamespacesFilename,                              'rb') as fp:
               self.program.component.namespaces.superSet                       = pickle.load(fp)

          with open(self.sharedCodesSupersetNamespacesFilename,                            'rb') as fp:
               self.program.sharedCodes.namespaces.superSet                     = pickle.load(fp)


          with open(self.carvedoutProgramExcludedNamespacesFromSupersetNamespacesFilename, 'rb') as fp:              
               self.program.carvedoutProgram.namespaces.excluded_from_superSet  = pickle.load(fp)
          
          with open(self.componentExcludedNamespacesFromSupersetNamespacesFilename,        'rb') as fp:  
               self.program.component.namespaces.excluded_from_superSet         = pickle.load(fp)
            
          with open(self.sharedCodesExcludedNamespacesFromSupersetNamespacesFilename,      'rb') as fp:              
               self.program.sharedCodes.namespaces.excluded_from_superSet       = pickle.load(fp)


          with open(self.carvedoutProgramCalledSubroutinesFilename,                       'rb') as fp:
               self.program.carvedoutProgram.calledSubroutinesFullNames         = pickle.load(fp)

          with open(self.componentCalledSubroutinesFilename,                              'rb') as fp:
               self.program.component.calledSubroutinesFullNames                = pickle.load(fp)      


          with open(self.carvedoutProgramSupersetVariablesFilename,                       'rb') as fp:
               self.program.carvedoutProgram.supersetGlobalVariablesFullNames   = pickle.load(fp)

          with open(self.componentSupersetVariablesFilename,                              'rb') as fp:
               self.program.component.supersetGlobalVariablesFullNames          = pickle.load(fp)      

          with open(self.sharedCodesSupersetVariablesFilename,                            'rb') as fp:
               self.program.sharedCodes.supersetGlobalVariablesFullNames        = pickle.load(fp)


          with open(self.carvedoutProgramSupersetDerivedTypesFilename,                    'rb') as fp:
               self.program.carvedoutProgram.supersetGlobalDerivedTypesFullNames = pickle.load(fp)

          with open(self.componentSupersetDerivedTypesFilename,                           'rb') as fp:
               self.program.component.supersetGlobalDerivedTypesFullNames        = pickle.load(fp)      

 
      def printResults_Overviw(self):
          self.printNamespacesOverview()

      def printResults(self):
          self.loadResults()
          self.printNamespaces()
          self.printVariables()

      def printResultsOverview(self):
          totalProgramNamespaces = set()
          totalProgramNamespaces.update(set(self.program.carvedoutProgram.namespaces.superSet              ))
          totalProgramNamespaces.update(set(self.program.component.namespaces.superSet                     ))
          totalProgramNamespaces.update(set(self.program.carvedoutProgram.namespaces.excluded_from_superSet))
          totalProgramNamespaces.update(set(self.program.component.namespaces.excluded_from_superSet       ))

          print("**** Analysis Completed!")         
          print("**** A total number of " + str(len(totalProgramNamespaces)) + " namespaces were analysed.")
          print("**** The dedependency information was extracted from the source codes of the program successfully!")
          print("**** The namespace dependence graph (NDG) was also created.")
          print("**** Run fortextraction and fortcleaning tools to complete the process of the component extraction.")

      def printNamespaces(self):
          print("\n\n\n*********************************** Before Cleaning ******************************************")
          print("***** CarvedoutProgram Superset Namespaces:")
          namespacePrintingNames = list()
          for namespaceName in self.program.carvedoutProgram.namespaces.superSet:
              namespacePrintingNames.append(self.sourceFiles.getNamespacePrintingName_from_namespaceName(namespaceName))
          print(namespacePrintingNames, len(namespacePrintingNames))

          print("\n***** Component Superset Namespaces:")
          namespacePrintingNames = list()
          for namespaceName in self.program.component.namespaces.superSet:
              namespacePrintingNames.append(self.sourceFiles.getNamespacePrintingName_from_namespaceName(namespaceName))
          print(namespacePrintingNames, len(namespacePrintingNames))

          print("\n***** Shared Superset Namespaces:")
          namespacePrintingNames = list()
          for namespaceName in self.program.sharedCodes.namespaces.superSet:
              namespacePrintingNames.append(self.sourceFiles.getNamespacePrintingName_from_namespaceName(namespaceName))
          print(namespacePrintingNames, len(namespacePrintingNames))

          print("\n\n*****  Excluded Namesapces from CarvedoutProgram:")
          print(self.program.carvedoutProgram.namespaces.excluded_from_superSet, len(self.program.carvedoutProgram.namespaces.excluded_from_superSet))
              
          print("\n*****  Excluded Namespaces from Component:")
          print(self.program.component.namespaces.excluded_from_superSet, len(self.program.component.namespaces.excluded_from_superSet))

          print("\n")
          print("***** CarvedoutProgram Superset Namespaces: ", len(self.program.carvedoutProgram.namespaces.superSet))
          print("***** Component Superset Namespaces       : ", len(self.program.component.namespaces.superSet       ))
          print("***** Shared Superset Namespaces          : ", len(self.program.sharedCodes.namespaces.superSet     ))

      def printVariables(self):
          print("\n")
          print("***** CarvedoutProgram Global Variables : ", len(set(self.program.carvedoutProgram.supersetGlobalVariablesFullNames)))
          print("***** Component Global Variables        : ", len(set(self.program.component.supersetGlobalVariablesFullNames       )))
          print("***** Shared Global Variables           : ", len(set(self.program.sharedCodes.supersetGlobalVariablesFullNames     )))


class SupersetSubcomponentExtraction():
      def __init__(self, sourceFiles, ndg, analysisConfig, graphBuilder):
          self.sourceFiles                            = sourceFiles
          self.ndg                                    = ndg
          self.analysisConfig                         = analysisConfig
          self.graphBuilder                           = graphBuilder
          self.subcomponent                           = None
          self.visitedExcludedNamespacesPrintingNames = set()

      def resetResults(self):
          self.visitedExcludedNamespacesPrintingNames = set()

          if self.subcomponent is None:
             return

          self.subcomponent.namespaces.superSet                 = list()
          self.subcomponent.namespaces.excluded_from_superSet   = set()
          self.subcomponent.subroutines                         = set()
          self.subcomponent.globalVariables                     = set()

          self.subcomponent.calledSubroutinesFullNames          = set()
          self.subcomponent.globalVariablesFullNames            = set()
          self.subcomponent.globalInterfacesFullNames           = set()
          self.subcomponent.supersetGlobalDerivedTypesFullNames = set()
 
      def getResults(self, supersetSubcomponent):
          self.subcomponent = supersetSubcomponent
          self.resetResults()
          self.getParts()

      def getParts(self):
          self.getCalledSubroutines()
          self.getGlobalInterfaces ()
          self.getNamespaces       ()
          self.getGlobalVariables  ()

      def getCalledSubroutines(self):
          if self.subcomponent is None:
             return
 
          self.subcomponent.subroutines                = set()
          self.subcomponent.calledSubroutinesFullNames = set()
 
          self.subcomponent.callGraph = CallGraph(self.ndg)
          callGraph                   = self.subcomponent.callGraph
          isCarveoutProgram           = self.subcomponent.isCarveoutProgram
 
          if isCarveoutProgram:
              subcomponentEntrypointFullName = self.subcomponent.entrypointsFullNames
              self.subcomponent.callGraph    = self.graphBuilder.buildCallGraph(subcomponentEntrypointFullName, callGraph, isCarveoutProgram, clear = self.analysisConfig.getClearCache())
              self.visitedExcludedNamespacesPrintingNames.update(self.graphBuilder.getVisitedExcludedNamespacesPrintingNames())
          else:
              componentImaginaryMasterEntrypointFullName = SubroutineFullName(ComponentImaginaryMasterEntrypointFullName)
              self.subcomponent.callGraph.addSubroutine(componentImaginaryMasterEntrypointFullName)
 
              for subcomponentEntrypointFullName in self.subcomponent.entrypointsFullNames:
                  self.subcomponent.callGraph.addCall(componentImaginaryMasterEntrypointFullName, subcomponentEntrypointFullName, -1, 0);
                  self.subcomponent.callGraph = self.graphBuilder.buildCallGraph(subcomponentEntrypointFullName, callGraph, isCarveoutProgram, clear = self.analysisConfig.getClearCache())
                  self.visitedExcludedNamespacesPrintingNames.update(self.graphBuilder.getVisitedExcludedNamespacesPrintingNames())
                  self.ndg.addEdge(ComponentImaginaryMasterEntrypointNameSpace, subcomponentEntrypointFullName.getNameSpace())
 
          self.subcomponent.subroutines                = self.subcomponent.callGraph.getAllSubroutineNames()
          self.subcomponent.calledSubroutinesFullNames = [subroutineFullName._name.upper() for subroutineFullName in self.subcomponent.subroutines]

      def getGlobalVariables(self):
          self.subcomponent.supersetGlobalVariablesFullNames = set()

          for subroutine in self.subcomponent.subroutines:
              namespacePrintingName = subroutine.getNameSpacePrintingName()                                                 
              module                = self.sourceFiles.getModule_from_nameSpacePrintingName(namespacePrintingName)
              if module is None:
                  continue
              for varName, var in module.getVariables().items():                   
                  if var.isParameter():
                      continue
                  self.subcomponent.supersetGlobalVariablesFullNames.add(var.getFullName())

      def getGlobalInterfaces(self):
          if self.subcomponent is None:
              return
          self.subcomponent.globalInterfacesFullNames           = set()
          self.subcomponent.supersetGlobalDerivedTypesFullNames = set()
          self.subcomponent.namespaces.excluded_from_superSet   = set()

          callGraph    = self.subcomponent.callGraph
          self.printer = UsedModuleNamePrinter(self.sourceFiles, self.ndg, self.analysisConfig)

          isCarveoutProgram = self.subcomponent.isCarveoutProgram
          if isCarveoutProgram:
              subcomponentEntrypointFullName             = self.subcomponent.entrypointsFullNames
              interfaces, types, requiredExcludedModules = self.printer.printUses(subcomponentEntrypointFullName, isCarveoutProgram, callGraph.getGlobalNamespaceSubprograms())

              self.subcomponent.namespaces.excluded_from_superSet.update(requiredExcludedModules)
          else:
              for subcomponentEntrypointFullName in self.subcomponent.entrypointsFullNames:
                  interfaces, types, requiredExcludedModules = self.printer.printUses(subcomponentEntrypointFullName, isCarveoutProgram, callGraph.getGlobalNamespaceSubprograms())
                  self.subcomponent.namespaces.excluded_from_superSet.update(requiredExcludedModules)

          for interfaceName, interface in interfaces.items():
              for procedureName in interface.getProcedures():
                  interfaceProcedureFullName = "__" + interface.getModuleName().upper() + "_MOD_" + procedureName.upper()
                  if interfaceProcedureFullName in self.subcomponent.calledSubroutinesFullNames:
                      interfaceFullName = interface.getFullName()
                      self.subcomponent.globalInterfacesFullNames.add(interfaceFullName.upper())
                      break 

          for typeName, typeList in types.items():
              for typE in typeList:  
                   self.subcomponent.supersetGlobalDerivedTypesFullNames.add(typE.getFullName())  

      def getNamespaces(self):
          if self.subcomponent is None:
              return
          self.subcomponent.namespaces.superSet= list()

          isCarveoutProgram = self.subcomponent.isCarveoutProgram
          if isCarveoutProgram:
              subcomponentEntrypointFullName  = self.subcomponent.entrypointsFullNames
              subcomponentEntrypointNamespace = subcomponentEntrypointFullName.getNameSpace()
          else:
              subcomponentEntrypointNamespace = ComponentImaginaryMasterEntrypointNameSpace

          namespaceNames = self.ndg.topologocalsort(subcomponentEntrypointNamespace)

          for namespaceName in namespaceNames:
              if namespaceName == ComponentImaginaryMasterEntrypointNameSpace:
                  continue

              if namespaceName in self.analysisConfig.getExcludeModulesInAnalysis():
                  self.subcomponent.namespaces.excluded_from_superSet.add(namespaceName)
                  continue

              globalPos = namespaceName.find(FortranProgramGlobalNameSpace)
              if globalPos > -1:
                  subroutineName = namespaceName[10:]
                  if subroutineName in self.analysisConfig.getExcludedSubroutinesInAnalysis():
                      self.program.carvedoutProgram.namespaces.excluded_from_superSet.add(namespaceName)
                      continue

              self.subcomponent.namespaces.superSet.append(namespaceName)

          for visitedExcludedNamespacePrintingName in self.visitedExcludedNamespacesPrintingNames:
              self.subcomponent.namespaces.excluded_from_superSet.add(visitedExcludedNamespacePrintingName)
