# coding=utf8
'''
@author: Mohammad Reza Heidari
'''

import copy
from source      import UseStatementParts
from collections import defaultdict
from printout    import printWarning
from dependency  import ContainerDependenciesDetector

class ModuleWrongDependencyDetector():
    def __init__(self, sourceFiles, analysisConfig, program):
        self.__sourceFiles      = sourceFiles
        self.__analysisConfig   = analysisConfig
        self.__program          = program

        self.__component        = self.__program.component
        self.__carvedoutProgram = self.__program.carvedoutProgram
        self.__moduleName       = None
        self.__module           = None

        self.moduleWrongDependencies_wrt_component          = defaultdict(UseStatementParts)
        self.moduleWrongDependencies_wrt_carvedoutprogram   = defaultdict(UseStatementParts)
        self.moduleCorrectDependencies_wrt_component        = defaultdict(UseStatementParts)
        self.moduleCorrectDependencies_wrt_carvedoutprogram = defaultdict(UseStatementParts)

    def printResults(self, namespacesPrintingName):
        self.getResults(namespacesPrintingName)

        print("\n\n---------------------------------------------------------------")
        print("Wrong Dependencies w.r.t. the Component:")
        for statement, parts in self.moduleWrongDependencies_wrt_component.items():
            print(statement, "--> ", parts.entities)

        print("\n")
        print("Wrong Dependencies w.r.t. the Carvedout Program:")
        for statement, parts in self.moduleWrongDependencies_wrt_carvedoutprogram.items():
            print(statement, "--> ", parts.entities)

        print("\n\n---------------------------------------------------------------")
        print("Correct Dependencies w.r.t. the Component:")
        for statement, parts in self.moduleCorrectDependencies_wrt_component.items():
            print(statement, "--> ", parts.entities)

        print("\n")
        print("Correct Dependencies w.r.t. the Carvedout Program:")
        for statement, parts in self.moduleCorrectDependencies_wrt_carvedoutprogram.items():
            print(statement, "--> ", parts.entities)

    def getResults(self, namespacesPrintingName):
        self.__module = self.__sourceFiles.getModule_from_nameSpacePrintingName(namespacesPrintingName)

        self.moduleWrongDependencies_wrt_component          = defaultdict(UseStatementParts)
        self.moduleWrongDependencies_wrt_carvedoutprogram   = defaultdict(UseStatementParts)
        self.moduleCorrectDependencies_wrt_component        = defaultdict(UseStatementParts)
        self.moduleCorrectDependencies_wrt_carvedoutprogram = defaultdict(UseStatementParts)

        if self.__module is None:
            printWarning('Module not found: ' + str(namespacesPrintingName), 'ModuleWrongDependencyDetector')
            return
        self.__moduleName = self.__module.getName()

        self.moduleWrongDependencies_wrt_component,        self.moduleCorrectDependencies_wrt_component        = self.findWrongDependencies(self.__component)
        self.moduleWrongDependencies_wrt_carvedoutprogram, self.moduleCorrectDependencies_wrt_carvedoutprogram = self.findWrongDependencies(self.__carvedoutProgram)

    def findWrongDependencies(self, programSubcomponent):
        self.__moduleDependenciesDetector = ContainerDependenciesDetector(self.__analysisConfig, self.__module, programSubcomponent, self.__sourceFiles)
        allUseStatements = self.__moduleDependenciesDetector.getDependencies_wrt_Subcomponent()

        globalSubroutinesFullNames = set([subroutineFullName.upper()      for subroutineFullName      in programSubcomponent.calledSubroutinesFullNames ])
        globalInterfacesFullNames  = set([globalInterfaceFullName.upper() for globalInterfaceFullName in programSubcomponent.globalInterfacesFullNames  ])
        globalVariablesFullnames   = set([var.upper()                     for var                     in programSubcomponent.globalVariablesFullNames   ])
        globalUsedDerivedTypes     = set([typE.upper()                    for typE                    in programSubcomponent.globalDerivedTypesFullNames])
        globalParametersFullnames  = set([globalParameterFullName.upper() for globalParameterFullName in programSubcomponent.globalParametersFullNames  ])

        globalUsedEntities = set()
        globalUsedEntities.update(globalSubroutinesFullNames)
        globalUsedEntities.update(globalInterfacesFullNames )
        globalUsedEntities.update(globalVariablesFullnames  )
        globalUsedEntities.update(globalUsedDerivedTypes    )
        globalUsedEntities.update(globalParametersFullnames )

        wrongDependencies   = defaultdict(UseStatementParts)
        correctDependencies = defaultdict(UseStatementParts)
        for statement, parts in copy.deepcopy(allUseStatements.items()):
            for usedEntity in parts.entities:
                if usedEntity.upper().find("OPERATOR") > -1:
                    #ToDo suppor OPERATOR alaising later
                    continue

                if usedEntity.find('=>') > -1:                                                                                                                                                                                    
                    names    = usedEntity.split('=>')                                                                                                                                                                                
                    alias    = names[0].strip()                                                                                                                                                                                    
                    original = names[1].strip()                                                                                                                                                                                 
                    entity   = original
                else:                                                                                                                                                                                                           
                    entity = usedEntity

                entity = "__" + parts.moduleName.upper() + "_MOD_" + entity.upper()

                if entity.upper() not in globalUsedEntities:
                    wrongDependencies[statement].moduleName = parts.moduleName
                    wrongDependencies[statement].entities.append(usedEntity)
                else:
                    correctDependencies[statement].moduleName = parts.moduleName
                    correctDependencies[statement].entities.append(usedEntity)

        return(wrongDependencies, correctDependencies)
